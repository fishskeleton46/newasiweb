$ = jQuery.noConflict();

$(window).on("load", function () {

  "use strict";

  /* ===================================
     Loading Timeout
  ====================================== */

  setTimeout(function () {
    $(".loader").fadeOut("slow");
  }, 1000);

  let url = window.location.href.split('#');
  if (url.length === 2) {
    if (url[1] === 'bpo') {
      let top = document.getElementById('work').offsetTop;
      $('#bpo_modal').modal('show');
      setTimeout(function () {
        window.scrollTo(0, top);
      }, 500);
    }
  }

});

jQuery(function ($) {

  "use strict";

  /* ===================================
     Scroll
  ====================================== */

  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 220) { // Set position from top to add class
      $('header').addClass('header-appear');
    }
    else {
      $('header').removeClass('header-appear');
    }
  });

  $(".progress-bar").each(function () {
    $(this).appear(function () {
      $(this).animate({width: $(this).attr("aria-valuenow") + "%"}, 3000)
    });
  });

  $('.count').each(function () {
    $(this).appear(function () {
      $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
      }, {
        duration: 3000,
        easing: 'swing',
        step: function (now) {
          $(this).text(Math.ceil(now));
        }
      });
    });
  });

  // scroll to appear
  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 500)
      $('.scroll-top-arrow').fadeIn('slow');
    else
      $('.scroll-top-arrow').fadeOut('slow');
  });

  // fixing bottom nav to top on scrolling
  var $fixednav = $(".bottom-nav");
  $(window).on("scroll", function () {
    var $heightcalc = $(window).height() - $fixednav.height();
    if ($(this).scrollTop() > $heightcalc) {
      $fixednav.addClass("navbar-bottom-top");
    } else {
      $fixednav.removeClass("navbar-bottom-top");
    }
  });

  // click event to scroll to top
  $(document).on('click', '.scroll-top-arrow', function () {
    $('html, body').animate({scrollTop: 0}, 800);
    return false;
  });


  // scroll sections
  $(".scroll").on("click", function (event) {
    event.preventDefault();
    $("html,body").animate({
      scrollTop: $(this.hash).offset().top - 60
    }, 1200);
  });

  /* ==================================
     Side Menu
  ===================================== */
  if ($("#sidemenu_toggle").length) {
    $("#sidemenu_toggle").on("click", function () {
      $(".pushwrap").toggleClass("active");
      $(".side-menu").addClass("side-menu-active");
      $("#close_side_menu").fadeIn(700);
    }), $("#close_side_menu").on("click", function () {
      $(".side-menu").removeClass("side-menu-active");
      $(this).fadeOut(200);
      $(".pushwrap").removeClass("active");
    }), $(".side-nav .navbar-nav .nav-link").on("click", function () {
      $(".side-menu").removeClass("side-menu-active");
      $("#close_side_menu").fadeOut(200);
      $(".pushwrap").removeClass("active");
    }), $("#btn_sideNavClose").on("click", function () {
      $(".side-menu").removeClass("side-menu-active");
      $("#close_side_menu").fadeOut(200);
      $(".pushwrap").removeClass("active");
    });
  }

  if ($(".side-right-btn").length) {

    $(".side-right-btn").click(function () {
      $(".navbar.navbar-right").toggleClass('show');
    });
    $(".navbar.navbar-right .navbar-nav .nav-link").click(function () {
      $(".navbar.navbar-right").toggleClass('show');
    });

  }

  /* =====================================
      Wow
  ======================================== */

  if ($(window).width() > 767) {
    var wow = new WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: false,
      live: true
    });
    new WOW().init();
  }

  /* ===================================
      Animated Cursor
  ====================================== */

  function animatedCursor() {

    if ($("#aimated-cursor").length) {

      var e = {x: 0, y: 0}, t = {x: 0, y: 0}, n = .25, o = !1, a = document.getElementById("cursor"),
        i = document.getElementById("cursor-loader");
      TweenLite.set(a, {xPercent: -50, yPercent: -50}), document.addEventListener("mousemove", function (t) {
        var n = window.pageYOffset || document.documentElement.scrollTop;
        e.x = t.pageX, e.y = t.pageY - n
      }), TweenLite.ticker.addEventListener("tick", function () {
        o || (t.x += (e.x - t.x) * n, t.y += (e.y - t.y) * n, TweenLite.set(a, {x: t.x, y: t.y}))
      }),
        $(".animated-wrap").mouseenter(function (e) {
          TweenMax.to(this, .3, {scale: 2}), TweenMax.to(a, .3, {
            scale: 2,
            borderWidth: "1px",
            opacity: .2
          }), TweenMax.to(i, .3, {
            scale: 2,
            borderWidth: "1px",
            top: 1,
            left: 1
          }), TweenMax.to($(this).children(), .3, {scale: .5}), o = !0
        }),
        $(".animated-wrap").mouseleave(function (e) {
          TweenMax.to(this, .3, {scale: 1}), TweenMax.to(a, .3, {
            scale: 1,
            borderWidth: "2px",
            opacity: 1
          }), TweenMax.to(i, .3, {
            scale: 1,
            borderWidth: "2px",
            top: 0,
            left: 0
          }), TweenMax.to($(this).children(), .3, {scale: 1, x: 0, y: 0}), o = !1
        }),
        $(".animated-wrap").mousemove(function (e) {
          var n, o, i, l, r, d, c, s, p, h, x, u, w, f, m;
          n = e, o = 2, i = this.getBoundingClientRect(), l = n.pageX - i.left, r = n.pageY - i.top, d = window.pageYOffset || document.documentElement.scrollTop, t.x = i.left + i.width / 2 + (l - i.width / 2) / o, t.y = i.top + i.height / 2 + (r - i.height / 2 - d) / o, TweenMax.to(a, .3, {
            x: t.x,
            y: t.y
          }), s = e, p = c = this, h = c.querySelector(".animated-element"), x = 20, u = p.getBoundingClientRect(), w = s.pageX - u.left, f = s.pageY - u.top, m = window.pageYOffset || document.documentElement.scrollTop, TweenMax.to(h, .3, {
            x: (w - u.width / 2) / u.width * x,
            y: (f - u.height / 2 - m) / u.height * x,
            ease: Power2.easeOut
          })
        }),
        $(".hide-cursor,.btn,.tp-bullets").mouseenter(function (e) {
          TweenMax.to("#cursor", .2, {borderWidth: "1px", scale: 2, opacity: 0})
        }), $(".hide-cursor,.btn,.tp-bullets").mouseleave(function (e) {
        TweenMax.to("#cursor", .3, {borderWidth: "2px", scale: 1, opacity: 1})
      }), $(".link").mouseenter(function (e) {
        TweenMax.to("#cursor", .2, {
          borderWidth: "0px",
          scale: 3,
          backgroundColor: "rgba(255, 255, 255, 0.27)",
          opacity: .15
        })
      }), $(".link").mouseleave(function (e) {
        TweenMax.to("#cursor", .3, {
          borderWidth: "2px",
          scale: 1,
          backgroundColor: "rgba(255, 255, 255, 0)",
          opacity: 1
        })
      })

    }

  }

  if ($(window).width() > 991) {
    setTimeout(function () {
      animatedCursor();
    }, 1000);
  }

  /* ===================================
      Owl Carousel
  ====================================== */

  // About Slider

  $("#laptop-slide").owlCarousel({
    items: 1,
    loop: true,
    dots: false,
    nav: false,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplay: true,
    autoplayTimeout: 3000,
    // mouseDrag: false,
    responsive: {
      1280: {
        items: 1,
      },
      600: {
        items: 1,
      },
      320: {
        items: 1,
      },
    }
  });

  // Careers Slider

  $("#careers-slider").owlCarousel({
    items: 3,
    dots: false,
    nav: false,
    autoplay: true,
    autoplayTimeout: 5000,
    rewind: true,
    responsive: {
      991: {
        items: 3,
      },
      767: {
        items: 2,
      },
      320: {
        items: 1,
      },
    }
  });

  // Testimonial slider

  $("#testimonial_slider").owlCarousel({
    items: 1,
    dots: true,
    nav: false,
  });

  /* =========================================
      Contact Us
  ============================================ */

  let notif_success = $('#notif_success');
  let text_success = $('#text_success');
  let notif_error = $('#notif_error');
  let text_error = $('#text_error');
  let ajax_loader = $('#ajax_loader');
  let btn_ok = $('#btn_ok');
  let btn_contact_us = $('#btn_contact_us');

  notif_success.hide();
  notif_error.hide();

  $('#contactForm').submit(function (e) {
    e.preventDefault();
    let postData = $(this).serializeArray();
    let formActionURL = $(this).attr('action');

    notif_success.hide();
    notif_error.hide();

    btn_contact_us.attr('disabled', true);
    btn_ok.attr('disabled', true);
    $('.close').hide();
    $('#thankyouModal').modal({
      'backdrop' : 'static',
      'keyboard' : false,
    });

    $.ajax({
      url: formActionURL,
      type: "POST",
      data: postData,
      beforeSend : function () {
        ajax_loader.show();
        btn_ok.attr('disabled', true);
        btn_ok.text('Please Wait ...');
      }
    }).done(function (data) {
      ajax_loader.hide();
      btn_ok.attr('disabled', false);
      btn_ok.text('OK');
      if (data['status'] === 'success'){
        text_success.text('Your message has been sent successfully.');
        notif_success.show();
        clearInput();
      } else {
        let message = (data['message']).replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
        text_error.text(message);
        notif_error.show();
        grecaptcha.reset();
      }
    }).fail(function (e) {
      ajax_loader.hide();
      btn_ok.attr('disabled', false);
      btn_ok.text('OK');
      console.log('error ' + e);
      grecaptcha.reset();
    });

    setTimeout(function () {
      btn_contact_us.attr('disabled', false);
    }, 2000);
  })

  function clearInput(){
    $('#name').val('');
    $('#job-title').val('');
    $('#company').val('');
    $('#phone').val('');
    $('#email').val('');
    $('#message').val('');
    document.getElementById('no-of-staff').selectedIndex = 0;
    grecaptcha.reset();
  }

  /* =========================================
      Cube Portfolio
  ============================================ */

  $('#js-grid-mosaic-flat').cubeportfolio({
    filters: '#js-filters-mosaic-flat',
    layoutMode: 'mosaic',
    sortByDimension: true,
    mediaQueries: [{
      width: 800,
      cols: 2
    }, {
      width: 576,
      cols: 1
    }],
    defaultFilter: '*',
    animationType: 'fadeOutTop',
    gapHorizontal: 0,
    gapVertical: 0,
    gridAdjustment: 'responsive',
    caption: 'zoom',

    // lightbox
    lightboxDelegate: '.cbp-lightbox',
    lightboxGallery: true,
    lightboxTitleSrc: 'data-title',

    plugins: {
      loadMore: {
        element: '#js-loadMore-mosaic-flat',
        action: 'click',
        loadItems: 3,
      }
    },
  });

  /* ===================================
      Revolution Slider
  ====================================== */

  $("#rev_slider_19_1").show().revolution({
    sliderType: "standard",
    jsFileLocation: "//localhost:82/revslider/revslider/public/assets/js/",
    sliderLayout: "fullscreen",
    dottedOverlay: "none",
    delay: 10000,
    navigation: {
      keyboardNavigation: "off",
      keyboard_direction: "horizontal",
      mouseScrollNavigation: "off",
      mouseScrollReverse: "default",
      onHoverStop: "off",
      bullets: {
        enable: true,
        hide_onmobile: true,
        hide_under: 767,
        style: "wexim",
        hide_onleave: false,
        direction: "vertical",
        h_align: "left",
        v_align: "center",
        h_offset: 30,
        v_offset: 0,
        space: 5,
        tmp: '<div class="tp-bullet-inner"></div><div class="tp-line"></div>'
      },
      touch: {
        touchenabled: "on",
        swipe_threshold: 75,
        swipe_min_touches: 1,
        swipe_direction: "horizontal",
        drag_block_vertical: false
      },
    },
    responsiveLevels: [1900, 1600, 1200, 1024, 778, 580],
    visibilityLevels: [1900, 1600, 1024, 778, 580],
    gridwidth: [1100, 1200, 1140, 960, 750, 480],
    gridheight: [868, 768, 960, 720],
    lazyType: "none",
    scrolleffect: {
      on_slidebg: "on",
    },
    parallax: {
      type: "mouse",
      origo: "slidercenter",
      speed: 2000,
      speedbg: 0,
      speedls: 0,
      levels: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
      disable_onmobile: "on"
    },
    shadow: 0,
    spinner: "off",
    stopLoop: "off",
    stopAfterLoops: -1,
    stopAtSlide: -1,
    shuffle: "off",
    autoHeight: "off",
    fullScreenAutoWidth: "off",
    fullScreenAlignForce: "off",
    fullScreenOffsetContainer: "",
    fullScreenOffset: "",
    disableProgressBar: "on",
    hideThumbsOnMobile: "on",
    hideSliderAtLimit: 0,
    hideCaptionAtLimit: 0,
    hideAllCaptionAtLilmit: 0,
    debugMode: false,
    fallbacks: {
      simplifyAll: "off",
      nextSlideOnWindowFocus: "off",
      disableFocusListener: false,
    }
  });

  /* ===================================
      BPO modal
  ====================================== */

  $('.cbp-item').click(function () {
    $('#bpo_card1').removeClass('bpo_out').addClass('bpo_fade_in');
    $('#bpo_card2').removeClass('bpo_fade_in').addClass('bpo_out');
  });
  $('#bpo_ac').click(function () {
    $('#bpo_card1').removeClass('bpo_fade_in').addClass('bpo_out');
    $('#bpo_card2').removeClass('bpo_out').addClass('bpo_fade_in');
  });
  $('#bpo_back').click(function () {
    $('#bpo_card1').removeClass('bpo_out').addClass('bpo_fade_in');
    $('#bpo_card2').removeClass('bpo_fade_in').addClass('bpo_out');
  });

});

