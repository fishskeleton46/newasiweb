$(document).ready(function(){
	var buildDeleteItem = function(){
		$('.remove-item').off('click');
		$('.remove-item').click(function(){
			var self = $(this);

			var target = self.attr('data-target-parent');

			self.parents(target).remove();
		});
	}

	$('.btn-add-items').click(function(){
		var self = $(this);

		var copy = self.data('copy');
		var paste = self.data('paste');

		var content = $(copy).html();

		$(paste).append($(copy).html());

		buildDeleteItem();
	});

	if($('.remove-item').length > 0){
		buildDeleteItem();
	}

	$('.custom-file input').on('change',function(){
		var self = $(this);
	    //get the file name
	    var fileName = self.val();
	    //replace the "Choose a file" label
		self.parents('.custom-file').find('.custom-file-label').html(fileName);
	})

	$('.remove-image').click(function(){
		var self = $(this);

		if(confirm('are you sure you want to delete this image')){
			$.ajax({
				url: self.attr('href'),
				type: 'GET',
				success: function(data) {
					var stan = jQuery.parseJSON(data);

					if(stan.status == 'success'){
						self.parents('.image-preview-box').fadeOut(1500, function(){
							$(this).remove();
						});
					}else{
						alert(stan.message)
					}
				},
				error: function(e) {
					alert('error deleting data')
				}
			});
		}

		return false;
	});

	if($("a.fancybox").length > 0){
		$("a.fancybox").fancybox();
	}

	$('a.alert-state').click(function() {
		var self = $(this);
		var body = self.attr('data-body');
		var href = self.attr('href');

		$('#alertBox .modal-body').text(body);
		$('#alertBox .approve-event').attr('href', href);

		$('#alertBox').modal('show');

		return false;
	});
});