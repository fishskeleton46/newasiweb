$ = jQuery.noConflict();

$(window).on("load", function () {

  "use strict";

  /* ===================================
     Loading Timeout
  ====================================== */

  setTimeout(function () {
    $(".loader").fadeOut("slow");
  }, 1000);

});

jQuery(function ($) {

  "use strict";

  /* ===================================
     Scroll
  ====================================== */

  // scroll to appear
  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 500)
      $('.scroll-top-arrow').fadeIn('slow');
    else
      $('.scroll-top-arrow').fadeOut('slow');
  });

  // click event to scroll to top
  $(document).on('click', '.scroll-top-arrow', function () {
    $('html, body').animate({scrollTop: 0}, 800);
    return false;
  });

  /* ==================================
     Side Menu
  ===================================== */
  if ($("#sidemenu_toggle").length) {
    $("#sidemenu_toggle").on("click", function () {
      $(".pushwrap").toggleClass("active");
      $(".side-menu").addClass("side-menu-active");
      $("#close_side_menu").fadeIn(700);
    }), $("#close_side_menu").on("click", function () {
      $(".side-menu").removeClass("side-menu-active");
      $(this).fadeOut(200);
      $(".pushwrap").removeClass("active");
    }), $(".side-nav .navbar-nav .nav-link").on("click", function () {
      $(".side-menu").removeClass("side-menu-active");
      $("#close_side_menu").fadeOut(200);
      $(".pushwrap").removeClass("active");
    }), $("#btn_sideNavClose").on("click", function () {
      $(".side-menu").removeClass("side-menu-active");
      $("#close_side_menu").fadeOut(200);
      $(".pushwrap").removeClass("active");
    });
  }

  if ($(".side-right-btn").length) {

    $(".side-right-btn").click(function () {
      $(".navbar.navbar-right").toggleClass('show');
    });
    $(".navbar.navbar-right .navbar-nav .nav-link").click(function () {
      $(".navbar.navbar-right").toggleClass('show');
    });

  }

  /* ===================================
      Animated Cursor
  ====================================== */

  function animatedCursor() {

    if ($("#aimated-cursor").length) {

      var e = {x: 0, y: 0}, t = {x: 0, y: 0}, n = .25, o = !1, a = document.getElementById("cursor"),
        i = document.getElementById("cursor-loader");
      TweenLite.set(a, {xPercent: -50, yPercent: -50}), document.addEventListener("mousemove", function (t) {
        var n = window.pageYOffset || document.documentElement.scrollTop;
        e.x = t.pageX, e.y = t.pageY - n
      }), TweenLite.ticker.addEventListener("tick", function () {
        o || (t.x += (e.x - t.x) * n, t.y += (e.y - t.y) * n, TweenLite.set(a, {x: t.x, y: t.y}))
      }),
        $(".animated-wrap").mouseenter(function (e) {
          TweenMax.to(this, .3, {scale: 2}), TweenMax.to(a, .3, {
            scale: 2,
            borderWidth: "1px",
            opacity: .2
          }), TweenMax.to(i, .3, {
            scale: 2,
            borderWidth: "1px",
            top: 1,
            left: 1
          }), TweenMax.to($(this).children(), .3, {scale: .5}), o = !0
        }),
        $(".animated-wrap").mouseleave(function (e) {
          TweenMax.to(this, .3, {scale: 1}), TweenMax.to(a, .3, {
            scale: 1,
            borderWidth: "2px",
            opacity: 1
          }), TweenMax.to(i, .3, {
            scale: 1,
            borderWidth: "2px",
            top: 0,
            left: 0
          }), TweenMax.to($(this).children(), .3, {scale: 1, x: 0, y: 0}), o = !1
        }),
        $(".animated-wrap").mousemove(function (e) {
          var n, o, i, l, r, d, c, s, p, h, x, u, w, f, m;
          n = e, o = 2, i = this.getBoundingClientRect(), l = n.pageX - i.left, r = n.pageY - i.top, d = window.pageYOffset || document.documentElement.scrollTop, t.x = i.left + i.width / 2 + (l - i.width / 2) / o, t.y = i.top + i.height / 2 + (r - i.height / 2 - d) / o, TweenMax.to(a, .3, {
            x: t.x,
            y: t.y
          }), s = e, p = c = this, h = c.querySelector(".animated-element"), x = 20, u = p.getBoundingClientRect(), w = s.pageX - u.left, f = s.pageY - u.top, m = window.pageYOffset || document.documentElement.scrollTop, TweenMax.to(h, .3, {
            x: (w - u.width / 2) / u.width * x,
            y: (f - u.height / 2 - m) / u.height * x,
            ease: Power2.easeOut
          })
        }),
        $(".hide-cursor,.btn,.tp-bullets").mouseenter(function (e) {
          TweenMax.to("#cursor", .2, {borderWidth: "1px", scale: 2, opacity: 0})
        }), $(".hide-cursor,.btn,.tp-bullets").mouseleave(function (e) {
        TweenMax.to("#cursor", .3, {borderWidth: "2px", scale: 1, opacity: 1})
      }), $(".link").mouseenter(function (e) {
        TweenMax.to("#cursor", .2, {
          borderWidth: "0px",
          scale: 3,
          backgroundColor: "rgba(255, 255, 255, 0.27)",
          opacity: .15
        })
      }), $(".link").mouseleave(function (e) {
        TweenMax.to("#cursor", .3, {
          borderWidth: "2px",
          scale: 1,
          backgroundColor: "rgba(255, 255, 255, 0)",
          opacity: 1
        })
      })

    }

  }

  if ($(window).width() > 991) {
    setTimeout(function () {
      animatedCursor();
    }, 1000);
  }

  /* ===================================
      Swiper
  ====================================== */

  var swiper = new Swiper('.swiper-container', {
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: 'auto',
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows : true,
    },
    autoplay: {
      delay: 10000,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

});

