$(document).ready(function(){
	var commonFunction = function(){
		$('.ajax-form button').off('click');
		$('.ajax-form button').click(function(){
			var self = $('.ajax-form');
			var url = self.attr('action');

			var formData = new FormData();

			if($('input[type="file"]').length > 0){
				var file_data = $('input[type="file"]')[0].files; // for multiple files
				for (var i = 0; i < file_data.length; i++){
					formData.append("file_" + i, file_data[i]);
				}
			}
			
			var other_data = self.serializeArray();
			$.each(other_data,function(key, input){
				formData.append(input.name, input.value);
			});

			var opts = {
			    url: url,
			    data: formData,
			    cache: false,
			    contentType: false,
			    processData: false,
			    method: 'POST',
			    type: 'POST', // For jQuery < 1.9
			    success: function(data){
			        var content = $(data).filter('.ajax-form');
			        var content_find = $(data).find('.ajax-form');

			        var real_content = '';
			        if(typeof content.html() != 'undefined'){
			        	real_content = content.html();
			        }else if(typeof content_find.html() != 'undefined'){
			        	real_content = content_find.html();
			        }

					$('.ajax-form').html(real_content);

					commonFunction();
			    }
			};

			$.ajax(opts);

			return false;
		});

		$('.custom-file input').on('change',function(){
			var self = $(this);
		    //get the file name
		    var fileName = self.val();
		    //replace the "Choose a file" label
			self.parents('.custom-file').find('.custom-file-label').html(fileName);
		})
	}

	$('.ajax-modal').click(function(){
		var self = $(this);
		var url = self.attr('data-url');
		var title = self.attr('data-title');
		var id_body = self.attr('data-id-body');

		$.ajax({
			url: url,
			type: 'GET',
			success: function(data) {
				$('#ajaxModal .modal-title').text(title);
				$('#ajaxModal .modal-body').attr('id', id_body);
				$('#ajaxModal .modal-body').html(data);
				
				$('#ajaxModal').modal('show');

				commonFunction();
			},
			error: function(e) {
				alert('There is something wrong :(')
			}
		});
	});

	commonFunction();
});