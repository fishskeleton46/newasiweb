<?php
    $facebook = (isset($data['facebook'])) ?$data['facebook'] : '';
    $twitter = (isset($data['twitter'])) ?$data['twitter'] : '';
    $instagram = (isset($data['instagram'])) ?$data['instagram'] : '';
    $youtube = (isset($data['youtube'])) ?$data['youtube'] : '';
    $linkedin = (isset($data['linkedin'])) ?$data['linkedin'] : '';

    echo form_open_multipart('access/social_media');
?>
  <div class="row">
    <div class="col-lg-8">
      <?php $this->view('element/setting_navigation'); ?>
      <div class="card mb-4">
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Facebook</label>
            <?php
                echo form_input('setting[facebook]', $facebook, array(
                  'class' => 'form-control', 
                  'placeholder' => 'Enter Facebook'
                ));
            ?>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Twitter</label>
            <?php
                echo form_input('setting[twitter]', $twitter, array(
                  'class' => 'form-control', 
                  'placeholder' => 'Enter Twitter'
                ));
            ?>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Linkedin</label>
            <?php
                echo form_input('setting[linkedin]', $linkedin, array(
                  'class' => 'form-control', 
                  'placeholder' => 'Enter Linkedin'
                ));
            ?>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Youtube</label>
            <?php
                echo form_input('setting[youtube]', $youtube, array(
                  'class' => 'form-control', 
                  'placeholder' => 'Enter Youtube'
                ));
            ?>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Instagram</label>
            <?php
                echo form_input('setting[instagram]', $instagram, array(
                  'class' => 'form-control', 
                  'placeholder' => 'Enter Instagram'
                ));
            ?>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card mb-4">
        <div class="card-header py-3">
          <h6 class="m-0">Action</h6>
        </div>
        <div class="card-body">
          <button type="submit" class="btn btn-success btn-block mb-2">
            <span class="text">Submit</span>
          </button>
        </div>
      </div>
    </div>
  </div>
</form>