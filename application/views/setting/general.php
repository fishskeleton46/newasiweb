<?php
  $email_contact = (isset($data['email-contact'])) ?$data['email-contact'] : '';

    echo form_open_multipart('access/general');
?>
  <div class="row">
    <div class="col-lg-8">
      <?php $this->view('element/setting_navigation'); ?>
      <div class="card mb-4">
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Email Contact</label>
            <?php
                echo form_input('setting[email-contact]', $email_contact, array(
                  'class' => 'form-control', 
                  'placeholder' => 'Enter Email Contact'
                ));
            ?>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card mb-4">
        <div class="card-header py-3">
          <h6 class="m-0">Action</h6>
        </div>
        <div class="card-body">
          <button type="submit" class="btn btn-success btn-block mb-2">
            <span class="text">Submit</span>
          </button>
        </div>
      </div>
    </div>
  </div>
</form>