<?php
    echo form_open_multipart('access/setting');
?>
    <div class="row">
      <div class="col-lg-8">
        <?php $this->view('element/setting_navigation'); ?>
        <div class="card mb-4">
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Old Password</label>
              <?php
                  echo form_password('old_password', '', array(
                    'class' => 'form-control', 
                    'placeholder' => 'Enter Old Password'
                  ));

                  echo form_error('old_password', '<div class="text-sm-left text-danger">', '</div>');
              ?>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">New Password</label>
              <?php
                  echo form_password('new_password', '', array(
                    'class' => 'form-control', 
                    'placeholder' => 'Enter New Password'
                  ));

                  echo form_error('new_password', '<div class="text-sm-left text-danger">', '</div>');
              ?>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Confirm Password</label>
              <?php
                  echo form_password('confirm_password', '', array(
                    'class' => 'form-control', 
                    'placeholder' => 'Enter Confirm Password'
                  ));

                  echo form_error('confirm_password', '<div class="text-sm-left text-danger">', '</div>');
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card mb-4">
          <div class="card-header py-3">
            <h6 class="m-0">Action</h6>
          </div>
          <div class="card-body">
            <button type="submit" class="btn btn-success btn-block mb-2">
              <span class="text">Submit</span>
            </button>
          </div>
        </div>
      </div>
  </div>
</form>