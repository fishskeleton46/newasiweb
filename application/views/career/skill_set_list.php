<?php
    $skill_name_val   = (isset($value_skill['name'])) ? $value_skill['name'] : '';
    $skill_value_val  = (isset($value_skill['value'])) ? $value_skill['value'] : '';
?>
<div class="item-skill">
  <div class="form-row mt-3 align-items-center">
    <div class="col-sm-6">
      <input type="text" name="skill_name[]" class="form-control" placeholder="Skill Name" value="<?php echo $skill_name_val;?>">
    </div>
    <div class="col-sm-3">
      <div class="input-group">
        <input type="text" name="skill_value[]" class="form-control" placeholder="Value" value="<?php echo $skill_value_val;?>">
        <div class="input-group-append">
          <div class="input-group-text">%</div>
        </div>
      </div>
    </div>
    <div class="col-sm-3">
      <a href="javascript:void(0)" class="remove-item btn btn-danger" data-target-parent=".item-skill"><i class="fas fa-minus-square"></i></a>
    </div>
  </div>
</div>