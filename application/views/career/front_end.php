<?php
  if(!empty($career)){
?>
  <!-- Careers Start -->
  <section id="careers" class="bg-light">
    <div class="container">
      <!--Heading-->
      <div class="row wow fadeIn">
        <div class="col-md-12 text-center">
          <div class="title d-inline-block">
            <h2 class="gradient-text1 mb-3">We love breaking the HR consultant stereotype</h2>
            <p>We pride ourselves in doing meaningful work for great clients.</p>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div id="careers-slider" class="owl-carousel owl-theme wow fadeInUp">
            <!--Career "Senior Software Engineer" -->
            <?php 
                foreach ($career as $key => $value) {
                  $type_job_with_location = array();

                  if(!empty($value->location)){
                    $type_job_with_location[] = $value->location;
                  }
                  if(!empty($value->type_job)){
                    $type_job_with_location[] = ucwords($value->type_job);
                  }

                  if(!empty($type_job_with_location)){
                    $type_job_with_location = implode(' / ', $type_job_with_location);
                  }
            ?>
            <div class="team-box item align-items-stretch ajax-modal" data-url="<?php echo site_url('career/ajx_career_info/'.$value->id_career) ?>" data-title="<?php echo $value->name_career; ?>" data-id-body="job<?php echo $value->id_career; ?>body">
              <!--Career Image-->
              <div class="team-image">
                <img src="<?php echo base_url().'assets/uploads/career/'.$value->image;?>" alt="image">
                <!--Career Overlay-->
                <div class="overlay center-block"></div>
              </div>
              <!--Career Text-->
              <div class="team-text">
                <h5><?php echo $value->name_career; ?></h5>
                <?php
                    if(!empty($type_job_with_location)){
                ?>
                <span class="alt-font"><?php echo $type_job_with_location; ?></span>
                <?php
                    }
                ?>
              </div>

              <?php 
                if(!empty($value->skill_set)){
                  $skill_set = json_decode($value->skill_set, true);
               ?>
              <!--Career Progress-->
              <ul class="team-progress text-left">
                <?php 
                  foreach ($skill_set as $key => $val_skill) {
                 ?>
                <!--Progress Item-->
                <li class="progress-item">
                  <h6><?php echo $val_skill['name']; ?><span class="float-right"><b class="count"><?php echo $val_skill['value']; ?></b>%</span></h6>
                  <div class="progress">
                    <span class="progress-bar" role="progressbar" aria-valuenow="<?php echo $val_skill['value']; ?>" aria-valuemin="0"
                          aria-valuemax="100"></span>
                  </div>
                </li>
                <?php 
                  }
                 ?>
              </ul>
              <?php 
                }
               ?>
            </div>
            <?php 
                }
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Careers End -->
<?php
  }
?>