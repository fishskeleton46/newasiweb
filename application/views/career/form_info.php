<?php
    $label_val                    = isset($career->label) ? $career->label : '';
    $description_information_val  = isset($career->description_information) ? $career->description_information : '';
    $order_row_val                = isset($career->order_row) ? $career->order_row : '';

    echo form_open_multipart('career/'.$action);
?>
    <div class="row">
      <div class="col-lg-8">
        
        <div class="card mb-4">
          <div class="card-body">
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
                <div class="form-group">
                  <label for="exampleInputEmail1">Label</label>
                  <?php
                      echo form_input('label', set_value('label', $label_val), array(
                        'class' => 'form-control', 
                        'placeholder' => 'Enter label'
                      ));

                      echo form_error('label', '<div class="text-sm-left text-danger">', '</div>');
                  ?>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <?php
                      echo form_textarea('description_information', '', array(
                        'class' => 'form-control ckeditor', 
                        'id' => 'editor-ckeditor'
                      ));

                      echo form_error('description_information', '<div class="text-sm-left text-danger">', '</div>');
                  ?>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Order</label>
                  <div class="form-row">
                    <div class="col-sm-3">
                      <?php
                          echo form_input('order_row', set_value('order_row', $order_row_val), array(
                            'class' => 'form-control text-center', 
                          ));
                      ?>
                    </div>
                  </div>
                  <?php
                      echo form_error('order_row', '<div class="text-sm-left text-danger">', '</div>');
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        
        <div class="card mb-4">
          <div class="card-header py-3">
            <h6 class="m-0">Action</h6>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6">
                <button type="submit" class="btn btn-success btn-block mb-2">
                  <span class="text">Submit</span>
                </button>
              </div>
              <div class="col-sm-6">
                <a href="<?php echo site_url('career');?>" class="btn btn-secondary btn-block">
                  <span class="text">Back</span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="sticky-top">
          <div class="card border-left-info shadow h-100 py-2 mb-3">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Career</div>
                  <p><?php echo $career->name_career?></p>
                  <small><?php echo $career->location?></small>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</form>

<script>
  document.getElementById('editor-ckeditor').innerHTML = "<?php echo set_value('description_information', $description_information_val); ?>";
</script>