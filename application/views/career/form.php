<?php
    $name_career_val  = isset($career->name_career) ? $career->name_career : '';
    $type_job_val     = isset($career->type_job) ? $career->type_job : '';
    $location_val     = isset($career->location) ? $career->location : '';
    $description_val  = isset($career->description) ? $career->description : '';
    $status_val       = isset($career->status) ? $career->status : 1;
    $image_val        = isset($career->image) ? $career->image : '';

    echo form_open_multipart('career/'.$action);
?>
    <div class="row">
      <div class="col-lg-8">
        <ul class="nav nav-tabs without-border-bottom" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="skill-set-tab" data-toggle="tab" href="#skill-set" role="tab" aria-controls="skill-set" aria-selected="false">Skill Set</a>
          </li>
        </ul>
        <div class="card mb-4">
          <div class="card-body">
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <?php
                      echo form_input('name_career', set_value('name_career', $name_career_val), array(
                        'class' => 'form-control', 
                        'placeholder' => 'Enter name career'
                      ));

                      echo form_error('name_career', '<div class="text-sm-left text-danger">', '</div>');
                  ?>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Location</label>
                  <?php
                      echo form_input('location', set_value('location', $location_val), array(
                        'class' => 'form-control', 
                        'placeholder' => 'Enter location'
                      ));

                      echo form_error('location', '<div class="text-sm-left text-danger">', '</div>');
                  ?>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Type Job</label>
                  <?php
                      echo form_dropdown('type_job', array(
                        '' => '-',
                        'full time' => 'Full Time',
                        'part time' => 'Part Time',
                        'internship' => 'Internship',
                      ), set_value('type_job', $type_job_val), array(
                        'class' => 'form-control', 
                      ));

                      echo form_error('type_job', '<div class="text-sm-left text-danger">', '</div>');
                  ?>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <?php
                      echo form_textarea('description', set_value('description', $description_val), array(
                        'class' => 'form-control', 
                        'rows'  => '4'
                      ));

                      echo form_error('description', '<div class="text-sm-left text-danger">', '</div>');
                  ?>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Image</label>
                  <div class="custom-file">
                    <?php
                        echo form_upload('image', '', array(
                          'class' => 'custom-file-input', 
                          'id' => 'customFile'
                        ));
                    ?>
                    <label class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                  <?php
                      echo form_error('image', '<div class="text-sm-left text-danger">', '</div>');
                  ?>
                </div>
              </div>
              <div class="tab-pane fade" id="skill-set" role="tabpanel" aria-labelledby="skill-set-tab">
                <div class="item-box">
                  <?php 
                    if(!empty($skill_set)){
                      $skill_set = json_decode($skill_set, true);

                      if(count($skill_set) > 0){
                        foreach ($skill_set as $key => $value) {
                          $this->view('career/skill_set_list', array(
                            'value_skill' => $value 
                          ));   
                        }
                      }else{
                        $this->view('career/skill_set_list'); 
                      }
                    }else{
                      $this->view('career/skill_set_list'); 
                    }
                  ?>
                </div>

                <hr>
                  
                <div class="add-section">
                  <a href="javascript:void(0)" class="btn btn-success btn-add-items" data-copy="#copy-item" data-paste=".item-box"><i class="fas fa-plus"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card mb-4">
          <div class="card-header py-3">
            <h6 class="m-0">Action</h6>
          </div>
          <div class="card-body">
            <div class="custom-control custom-switch mb-3">
              <input type="checkbox" class="custom-control-input" name="status" value="1" <?php echo (set_value('status', $status_val) == 1) ? 'checked="checked"' : '';?> id="customSwitch1">
              <label class="custom-control-label" for="customSwitch1">Status Active</label>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <button type="submit" class="btn btn-success btn-block mb-2">
                  <span class="text">Submit</span>
                </button>
              </div>
              <div class="col-sm-6">
                <a href="<?php echo site_url('career');?>" class="btn btn-secondary btn-block">
                  <span class="text">Back</span>
                </a>
              </div>
            </div>
          </div>
        </div>
        
        <?php
            if(!empty($image_val)){
        ?>
        <div class="card mb-4 image-preview-box">
          <div class="card-header py-3">
            <h6 class="m-0">Preview</h6>
          </div>
          <div class="card-body text-center">
            <a href="<?php echo base_url().'assets/uploads/career/'.$image_val;?>" class="fancybox">
              <img src="<?php echo base_url().'assets/uploads/career/'.$image_val;?>" class="img-thumbnail mb-3" alt="">
            </a>
            <a href="<?php echo site_url('career/delete_image/'.$id);?>" class="btn btn-danger btn-block remove-image"><i class="fas fa-trash-alt"></i> remove</a>
          </div>
        </div>
        <?php
            }
        ?>
      </div>
  </div>
</form>

<div class="hide" id="copy-item">
  <?php 
    $this->view('career/skill_set_list', array(
      'value_skill' => array(
        'name' => '',
        'value' => '',
      ) 
    )); 
  ?>
</div>