<div class="card shadow mb-4">
	<div class="col-sm-12 my-3">
		<form action="/career/search/career-index" class="form-inline float-right" method="POST">
			<div class="input-group">
				<input type="text" name="keyword" value="<?php echo $keyword;?>" class="form-control bg-light border-0 small" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
				<div class="input-group-append">
					<button class="btn btn-primary" type="button">
						<i class="fas fa-search fa-sm"></i>
					</button>
				</div>
            </div>
		</form>
	</div>
	
	<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Name</th>
			      <th scope="col">Location</th>
			      <th scope="col">Type</th>
			      <th scope="col">Status</th>
			      <th scope="col">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php
			  		if(!empty($career)){
			  			$idx = $this->uri->segment('3') + 1;
			  			foreach ($career as $key => $value) {
			  				$information = $value->information;
			  				$count_information = count($information);

			  				if($count_information > 0){
			  					$span = 'rowspan="'.($count_information+1).'"';
			  				}else{
			  					$span = '';
			  				}
			  	?>
			  	<tr>
			      <th scope="row" <?php echo $span; ?>><?php echo $idx++;?></th>
			      <td><?php echo $value->name_career;?></td>
			      <td><?php echo $value->location;?></td>
			      <td><?php echo ucwords($value->type_job);?></td>
			      <td>
			      	<?php 
			      		if(empty($value->status)){
			      			echo '<span class="badge badge-pill badge-danger">non-active</span>';
			      		}else{
			      			echo '<span class="badge badge-pill badge-success">active</span>';
			      		}
			      	?>			      		
			      </td>
			      <td>
					<div class="dropdown">
						<button class="btn btn-sm btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
						<div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -104px, 0px);">
							<a class="dropdown-item" href="<?php echo site_url('career/add_info/'.$value->id_career);?>">Add Info</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="<?php echo site_url('career/edit/'.$value->id_career);?>">Edit</a>
							<a class="dropdown-item alert-state" data-body="are you sure wan to delete this data?" href="<?php echo site_url('career/delete/'.$value->id_career);?>">Delete</a>
						</div>
					</div>
			      </td>
			    </tr>
			    <?php
			    		if($count_information > 0){
			    			foreach ($information as $key => $val_information) {
			    ?>
			    <tr>
			    	<td colspan="4">___<?php echo $val_information->label;?></td>
			    	<td>
					<div class="dropdown">
						<button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
						<div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -104px, 0px);">
							<a class="dropdown-item" href="<?php echo site_url('career/edit_info/'.$val_information->id_career_information);?>">Edit</a>
							<a class="dropdown-item alert-state" data-body="are you sure wan to delete this data?" href="<?php echo site_url('career/delete_info/'.$val_information->id_career_information);?>">Delete</a>
						</div>
					</div>
			      </td>
			    </tr>
			    <?php
			    			}
			    		}
			    ?>
			  	<?php
			  			}
			  		}else{
			  	?>
			  	<tr>
			      <td colspan="5" class="text-center">Data not available</td>
			    </tr>
			  	<?php
			  		}
			  	?>
			    
			  </tbody>
			</table>
		</div>
		<div class="mt-lg-0 mt-sm-3">
			<a href="<?php echo site_url('career/add');?>" class="btn btn-sm btn-primary mb-3 float-left"><i class="fas fa-plus"></i> Add Career</a>
			<?php 
				echo $this->pagination->create_links();
			?>
		</div>
	</div>
</div>