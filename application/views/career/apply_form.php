<?php echo form_open_multipart('career/ajx_career_info/'.$id, array('class' => 'contact-form ajax-form')); ?>
  <?php if($this->session->flashdata('error')){ ?>
    <div class="alert alert-danger" role="alert">
      <?php echo $this->session->flashdata('error'); ?>
    </div>
  <?php } ?>
  <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success" role="alert">
      <?php echo $this->session->flashdata('success'); ?>
    </div>
  <?php } ?>
  
  <div class="row" style="text-align: left;">

    <div class="col-12" id="result"></div>

    <div class="col-12 col-sm-9">
      <div class="form-group">
        <?php
            echo form_input('name', set_value('name'), array(
              'class' => 'form-control', 
              'placeholder' => 'Full Name',
              // 'required' => 'required'
            ));

            echo form_error('name', '<div class="text-sm-left text-danger">', '</div>');
        ?>
      </div>
    </div>
    <div class="col-12 col-sm-9">
      <div class="form-group">
        <?php
            echo form_input('phone', set_value('phone'), array(
              'type' => 'number',
              'class' => 'form-control', 
              'placeholder' => 'Phone',
              // 'required' => 'required'
            ));

            echo form_error('phone', '<div class="text-sm-left text-danger">', '</div>');
        ?>
      </div>
    </div>
    <div class="col-12 col-sm-9">
      <div class="form-group">
        <?php
            echo form_input('email', set_value('email'), array(
              'type' => 'email',
              'class' => 'form-control', 
              'placeholder' => 'Email',
              // 'required' => 'required'
            ));

            echo form_error('email', '<div class="text-sm-left text-danger">', '</div>');
        ?>
      </div>
    </div>
    <div class="col-12 col-sm-9">
      <p style="font-weight: 600;">Upload Resume</p>
      <div class="form-group custom-file">
        <?php
            echo form_upload('files', '', array(
              'class' => 'form-control custom-file-input', 
              // 'required' => 'required',
              'id' => 'resume'
            ));
        ?>
        <label class="custom-file-label" for="resume">Choose File</label>
      </div>
      <?php
          echo form_error('files', '<div class="text-sm-left text-danger">', '</div>');
      ?>
      <p style="font-style: italic">
        Files must be less than <span style="font-weight: 500">4 MB</span>.<br>
        Allowed file types: <span style="font-weight: 500">pdf doc docx</span>.
      </p>
    </div>
    <div class="col-12 col-sm-9">
      <?php 
        echo $captcha; 
        echo $script_captcha; 
      ?>
    </div>
    <div class="col-sm-12">
      <button type="submit" class="btn btn-large btn-gradient btn-rounded mt-4" id="submit_btn">
        <i class="fa fa-spinner fa-spin mr-2 d-none" aria-hidden="true"></i> <span>Submit</span>
      </button>
    </div>
  </div>
</form>