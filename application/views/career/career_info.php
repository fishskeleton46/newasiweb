<?php 
  $index = 1;
  $id_career = $career->id_career;
 ?>
<p><?php echo str_replace(PHP_EOL, '<br>', $career->description); ?></p>

<?php 
    $index = 1;
    if(!empty($career->information)){
      $idx = 1;
      foreach ($career->information as $key => $val_info) {
        $show = '';
        if($idx == 1){
          $show = 'show';
          $idx++;
        }

        $h_class = sprintf('job%sh%s', $id_career, $index);
        $c_class = sprintf('job%sc%s', $id_career, $index);
 ?>
<div class="card">
  <button class="btn card-header" id="<?php echo $h_class;?>" data-toggle="collapse" data-target="#<?php echo $c_class;?>" aria-expanded="true"
          aria-controls="<?php echo $h_class;?>">
    <?php echo $val_info->label; ?>
  </button>
  <div id="<?php echo $c_class;?>" class="collapse <?php echo $show; ?>" aria-labelledby="<?php echo $h_class;?>" data-parent="#job<?php echo $id_career; ?>body">
    <div class="card-body">
      <?php echo $val_info->description_information; ?>
    </div>
  </div>
</div>
<?php 
        $index++;
      }
    }

    $index++;

    $h_class = sprintf('job%sh%s', $id_career, $index);
    $c_class = sprintf('job%sc%s', $id_career, $index);
 ?>
<div class="card">
  <button class="btn card-header" id="<?php echo $h_class;?>" data-toggle="collapse" data-target="#<?php echo $c_class;?>" aria-expanded="true" aria-controls="<?php echo $h_class;?>">
    Apply Now
  </button>
  <div id="<?php echo $c_class;?>" class="collapse" aria-labelledby="<?php echo $h_class;?>" data-parent="#job<?php echo $id_career; ?>body">
    <div class="card-body">
      <?php $this->view('career/apply_form'); ?>
    </div>
  </div>
</div>
