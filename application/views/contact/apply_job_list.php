<div class="card shadow mb-4">
	<div class="col-sm-12 my-3">
		<form action="/contact/search/contact-apply_job" class="form-inline float-right" method="POST">
			<div class="input-group">
				<input type="text" name="keyword" value="<?php echo $keyword;?>" class="form-control bg-light border-0 small" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
				<div class="input-group-append">
					<button class="btn btn-primary" type="button">
						<i class="fas fa-search fa-sm"></i>
					</button>
				</div>
            </div>
		</form>
	</div>
	
	<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Name</th>
			      <th scope="col">Career</th>
			      <th scope="col">Phone</th>
			      <th scope="col">Email</th>
			      <th scope="col">Created</th>
			      <th scope="col">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php
			  		if(!empty($apply_job)){
			  			$idx = $this->uri->segment('3') + 1;
			  			foreach ($apply_job as $key => $value) {
			  	?>
			  	<tr>
			      <th scope="row"><?php echo $idx++;?></th>
			      <td><?php echo $value->name;?></td>
			      <td><?php echo $value->career->name_career;?></td>
			      <td><?php echo $value->phone;?></td>
			      <td><?php echo $value->email;?></td>
			      <td>
			      	<?php 
			      		echo $value->created;
			      	?>			      		
			      </td>
			      <td>
					<a href="<?php echo site_url('assets/uploads/files_cv/'.$value->files); ?>" class="btn btn-sm btn-info">Download CV</a>
			      </td>
			    </tr>
			  	<?php
			  			}
			  		}else{
			  	?>
			  	<tr>
			      <td colspan="7" class="text-center">Data not available</td>
			    </tr>
			  	<?php
			  		}
			  	?>
			    
			  </tbody>
			</table>
		</div>
		<div class="mt-lg-0 mt-sm-3">
			<?php 
				echo $this->pagination->create_links();
			?>
		</div>
	</div>
</div>