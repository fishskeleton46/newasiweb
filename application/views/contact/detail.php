<div class="row">
  <div class="col-lg-8">
    <div class="card mb-4">
      <div class="card-body">
        <h1 class="mb-3">Contact Detail</h1>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label class="font-weight-bold" for="">Name</label>
              <div><?php echo $contact->name ?></div>
            </div>
            <div class="form-group">
              <label class="font-weight-bold" for="">Company</label>
              <div><?php echo $contact->company ?></div>
            </div>
            <div class="form-group">
              <label class="font-weight-bold" for="">Mobile Phone</label>
              <div><?php echo $contact->phone ?></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="font-weight-bold" for="">Job Title</label>
              <div><?php echo $contact->job_title ?></div>
            </div>
            <div class="form-group">
              <label class="font-weight-bold" for="">Number of Employees</label>
              <div><?php echo $contact->no_of_staff ?></div>
            </div>
            <div class="form-group">
              <label class="font-weight-bold" for="">Email</label>
              <div><?php echo $contact->email ?></div>
            </div>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label class="font-weight-bold" for="">Message:</label>
              <div><?php echo $contact->message ?></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    
    <div class="card mb-4">
      <div class="card-header py-3">
        <h6 class="m-0">Action</h6>
      </div>
      <div class="card-body">
        <a href="<?php echo site_url('contact');?>" class="btn btn-secondary btn-block">
          <span class="text">Back</span>
        </a>
      </div>
    </div>
  </div>
</div>