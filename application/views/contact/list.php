<div class="card shadow mb-4">
	<div class="col-sm-12 my-3">
		<form action="/contact/search/contact-index" class="form-inline float-right" method="POST">
			<div class="input-group">
				<input type="text" name="keyword" value="<?php echo $keyword;?>" class="form-control bg-light border-0 small" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
				<div class="input-group-append">
					<button class="btn btn-primary" type="button">
						<i class="fas fa-search fa-sm"></i>
					</button>
				</div>
            </div>
		</form>
	</div>
	
	<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Name</th>
			      <th scope="col">Job Title</th>
			      <th scope="col">Company</th>
			      <th scope="col">Phone</th>
			      <th scope="col">Email</th>
			      <th scope="col">Status</th>
			      <th scope="col">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php
			  		if(!empty($contact)){
			  			$idx = $this->uri->segment('3') + 1;
			  			foreach ($contact as $key => $value) {
			  	?>
			  	<tr>
			      <th scope="row"><?php echo $idx++;?></th>
			      <td><?php echo $value->name;?></td>
			      <td><?php echo $value->job_title;?></td>
			      <td><?php echo $value->company;?></td>
			      <td><?php echo $value->phone;?></td>
			      <td><?php echo $value->email;?></td>
			      <td>
			      	<?php 
			      		if(empty($value->has_read)){
			      			echo '<span class="badge badge-pill badge-danger">not read</span>';
			      		}else{
			      			echo '<span class="badge badge-pill badge-success">read</span>';
			      		}
			      	?>			      		
			      </td>
			      <td>
					<a href="<?php echo site_url('contact/detail/'.$value->id_contact); ?>" class="btn btn-sm btn-info">Detail</a>
			      </td>
			    </tr>
			  	<?php
			  			}
			  		}else{
			  	?>
			  	<tr>
			      <td colspan="8" class="text-center">Data not available</td>
			    </tr>
			  	<?php
			  		}
			  	?>
			    
			  </tbody>
			</table>
		</div>
		<div class="mt-lg-0 mt-sm-3">
			<?php 
				echo $this->pagination->create_links();
			?>
		</div>
	</div>
</div>