<?php 
  $facebook = (isset($setting['facebook'])) ?$setting['facebook'] : '';
  $twitter = (isset($setting['twitter'])) ?$setting['twitter'] : '';
  $instagram = (isset($setting['instagram'])) ?$setting['instagram'] : '';
  $youtube = (isset($setting['youtube'])) ?$setting['youtube'] : '';
  $linkedin = (isset($setting['linkedin'])) ?$setting['linkedin'] : '';
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Page Title -->
  <title>ASI Asia Pacific | Making HR Simple</title>
  <!-- Favicon -->
  <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico">
  <!-- Animate -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.min.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
  <!-- Owl Carousel -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.theme.default.min.css">
  <!-- Cube Portfolio -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/cubeportfolio.min.css">
  <!-- Fancy Box -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.fancybox.min.css">
  <!-- REVOLUTION STYLE SHEETS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/rs-plugin/css/settings.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/rs-plugin/css/navigation.css">
  <!-- Style Sheet -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="90">

<!--Loader Start-->
<div class="loader">
  <div class="loader-inner">
    <div class="loader-blocks">
      <span class="block-1"></span>
      <span class="block-2"></span>
      <span class="block-3"></span>
      <span class="block-4"></span>
      <span class="block-5"></span>
      <span class="block-6"></span>
      <span class="block-7"></span>
      <span class="block-8"></span>
      <span class="block-9"></span>
      <span class="block-10"></span>
      <span class="block-11"></span>
      <span class="block-12"></span>
      <span class="block-13"></span>
      <span class="block-14"></span>
      <span class="block-15"></span>
      <span class="block-16"></span>
    </div>
  </div>
</div>
<!--Loader End-->

<!--Header Start-->
<header class="cursor-light">

  <!--Navigation-->
  <nav class="navbar navbar-top-default navbar-expand-lg navbar-gradient nav-icon">
    <div class="container">
      <a href="#home" title="Logo" class="logo link scroll">
        <!--Logo Default-->
        <img src="<?php echo base_url();?>assets/images/asi80.png" alt="logo" class="logo-dark default">
      </a>

      <!--Nav Links-->
      <div class="collapse navbar-collapse" id="wexim">
        <div class="navbar-nav ml-auto">
          <a class="nav-link link scroll" href="#home"><?php echo $this->lang->line('menu_home'); ?></a>
          <a class="nav-link link scroll" href="#about">About</a>
          <a class="nav-link link scroll" href="#work">Work</a>
          <a class="nav-link link scroll" href="#careers">Careers</a>
          <a class="nav-link link scroll" href="#contact">Contact</a>
          <span class="menu-line"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
        </div>
      </div>

      <!--Side Menu Button-->
      <a href="javascript:void(0)" class="d-inline-block parallax-btn sidemenu_btn" id="sidemenu_toggle">
        <div class="animated-wrap sidemenu_btn_inner">
          <div class="animated-element">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      </a>
    </div>
  </nav>

  <!--Side Nav-->
  <div class="side-menu">
    <div class="inner-wrapper">
      <span class="btn-close link" id="btn_sideNavClose"></span>
      <nav class="side-nav w-100">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link link scroll" href="#home"><?php echo $this->lang->line('menu_home'); ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link link scroll" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link link scroll" href="#work">Work</a>
          </li>
          <li class="nav-item">
            <a class="nav-link link scroll" href="#careers">Careers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link link scroll" href="#contact">Contact</a>
          </li>
        </ul>
      </nav>

      <div class="side-footer text-white w-100">
        <ul class="social-icons-simple">
          <?php 
              if(!empty($facebook)){
           ?>
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="<?php echo $facebook; ?>">
              <i class="fa fa-facebook"></i>
            </a>
          </li>
          <?php 
              }

              if(!empty($twitter)){
           ?>
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="<?php echo $twitter; ?>">
              <i class="fa fa-twitter"></i>
            </a>
          </li>
          <?php 
              }

              if(!empty($linkedin)){
           ?>
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="<?php echo $linkedin; ?>">
              <i class="fa fa-linkedin"></i>
            </a>
          </li>
          <?php 
              }

              if(!empty($youtube)){
           ?>
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="<?php echo $youtube; ?>">
              <i class="fa fa-youtube-play"></i>
            </a>
          </li>
          <?php 
              }

              if(!empty($instagram)){
           ?>
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="<?php echo $instagram; ?>">
              <i class="fa fa-instagram"></i>
            </a>
          </li>
          <?php 
              }
           ?>
        </ul>
      </div>
    </div>
  </div>
  <a id="close_side_menu" href="javascript:void(0);"></a>
  <!-- End side menu -->

</header>
<!--Header end-->

<!--slider-->
<section id="home" class="cursor-light p-0">
  <h2 class="d-none">hidden</h2>
  <div id="rev_slider_19_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="wexim_slider_01"
       data-source="gallery" style="background: transparent; padding: 0;">
    <!-- START REVOLUTION SLIDER 5.4.8.1 fullscreen mode -->
    <div id="rev_slider_19_1" class="rev_slider fullscreenbanner" style="display: none;" data-version="5.4.8.1">
      <ul>
        <!-- SLIDE  -->
        <li data-index="rs-1" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0"
            data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
            data-thumb="images/slide-img1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide"
            data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
            data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
          <!-- MAIN IMAGE -->
          <img src="<?php echo base_url();?>assets/images/slider-image1.jpg" alt="" data-bgposition="center center"
               data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->

          <!-- Overlay -->
          <div class="gradient-bg1 bg-overlay opacity-9 z-index-1"></div>

          <!-- LAYER NR. 2 -->
          <div class="tp-caption tp-resizeme"
               id="slide-91-layer-2"
               data-x="['left','left','center','center']" data-hoffset="['0','50','0','0']"
               data-y="['top','top','top','top']" data-voffset="['357','309','283','155']"
               data-fontsize="['36','36','32','28']"
               data-width="none"
               data-height="none"
               data-whitespace="nowrap"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":720,"split":"chars","splitdelay":0.05,"speed":900,"split_direction":"forward","frame":"0","from":"sX:0.8;sY:0.8;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['inherit','inherit','center','center']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 6; white-space: nowrap; font-size: 40px; line-height: 50px; font-weight: 500; color: #ffffff; letter-spacing: 0; font-family: Poppins, sans-serif;">
            <p class="text-white" style="font-size: inherit;">We specialize in HR technology</p>
            <p class="text-white" style="font-size: inherit;">for Recruitment and Assessment.</p>
          </div>

          <!-- LAYER NR. 4 -->
          <div class="tp-caption tp-resizeme"
               id="slide-91-layer-4"
               data-x="['left','left','center','center']" data-hoffset="['0','50','1','-1']"
               data-y="['top','top','middle','middle']" data-voffset="['484','447','0','0']"
               data-fontsize="['16','16','12','12']"
               data-lineheight="['22','22','18','12']"
               data-width="['601','530','450','450']"
               data-height="none"
               data-whitespace="normal"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":1680,"speed":900,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['inherit','inherit','center','center']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 8; min-width: 601px; max-width: 601px; white-space: normal; font-size: 16px; line-height: 22px; font-weight: 300; color: #ffffff; letter-spacing: 0; font-family: Roboto, sans-serif;">
            <p class="text-white" id="text-middle-slide-1">
              You deserve the best.<br>
              We empower our clients with the technology they deserve.<br>
              We teach large organizations how to recruit fast and effectively.
            </p>
          </div>

          <!-- LAYER NR. 12 -->
          <div class="tp-caption tp-resizeme"
               data-x="['left','left','center','center']" data-hoffset="['0','50','0','0']"
               data-y="['top','top','bottom','bottom']" data-voffset="['586','560','64','50']"
               data-width="['601','530','601','450']"
               data-height="none"
               data-whitespace="nowrap"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":2150,"speed":900,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['inherit','inherit','center','center']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 9; letter-spacing: .5px;">
            <a href="#contact" class="btn btn-large btn-rounded btn-white scroll">Enquire Now</a>
            <a href="https://www.youtube.com/watch?v=l_e34mkvv2A"
               class="cbp-caption cbp-lightbox btn btn-large btn-rounded btn-transparent-white">
              <span class="ti-control-play"></span> Watch the video
            </a>
          </div>

          <!-- LAYER NR. 6 -->
          <div class="tp-caption d-none d-lg-block tp-resizeme rs-parallaxlevel-1"
               id="slide-91-layer-7"
               data-x="['right','right','right','right']" data-hoffset="['-100','-50','-1200','-1200']"
               data-y="['middle','middle','middle','middle']" data-voffset="['0','0','-9','-27']"
               data-width="none"
               data-height="none"
               data-whitespace="normal"

               data-type="image"
               data-responsive_offset="on"

               data-frames='[{"delay":1810,"speed":900,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;",
                  "to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['inherit','inherit','inherit','inherit']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 10;">
            <div class="rs-looped rs-wave" data-speed="10" data-angle="0" data-radius="5px" data-origin="50% 50%">
              <img src="<?php echo base_url();?>assets/images/object1.png" alt="" data-ww="['576px','530px','462px','462px']"
                   data-hh="['566px','530px','454px','454px']" data-no-retina>
            </div>
          </div>

          <!-- LAYER NR. 7 -->
          <div class="tp-caption d-none d-lg-block tp-resizeme rs-parallaxlevel-2"
               id="slide-91-layer-8"
               data-x="['right','right','right','right']" data-hoffset="['-80','-60','-1200','-1200']"
               data-y="['middle','middle','middle','middle']" data-voffset="['70','70','20','2']"
               data-width="none"
               data-height="none"
               data-whitespace="normal"

               data-type="image"
               data-responsive_offset="on"

               data-frames='[{"delay":2330,"speed":900,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['inherit','inherit','inherit','inherit']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 11;">

            <div class="rs-looped rs-wave" data-speed="20" data-angle="0" data-radius="8px" data-origin="50% 50%">
              <svg width="500" height="495">
                <g>
                  <clipPath id="clipPolygon">
                    <polygon
                      points="186 13,164 17,150 19,118 27,92 36,78 44,59 57,39 78,26 98,18 118,14 133,12 143,9 159,9 174,12 191,15 207,18 219,22 231,28 244,38 267,50 290,65 313,82 337,97 356,109 369,126 385,138 397,156 411,176 422,192 427,211 431,226 433,244 431,258 428,272 423,286 417,298 409,315 397,336 374,349 357,363 335,380 306,394 277,405 250,414 224,423 196,427 178,429 162,429 140,426 122,420 100,413 86,404 72,391 57,373 42,355 32,333 24,307 18,278 13,249 11,207 11,182 14">
                    </polygon>
                  </clipPath>
                </g>
                <image clip-path="url(#clipPolygon)" height="100%" width="100%" xlink:href="<?php echo base_url();?>assets/images/svg-image1.jpg" />
              </svg>
            </div>

          </div>
        </li>

        <!-- SLIDE  -->
        <li data-index="rs-2" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0"
            data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
            data-thumb="images/slide-img2.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide"
            data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7=""
            data-param8="" data-param9="" data-param10="" data-description="">

          <!-- MAIN IMAGE -->
          <img src="<?php echo base_url();?>assets/images/slider-image2.jpg" alt="" data-bgposition="center center"
               data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->

          <!-- Overlay -->
          <div class="gradient-bg1 bg-overlay opacity-9 z-index-1"></div>

          <!-- LAYER NR. 8 -->
          <div class="tp-caption tp-resizeme"
               id="slide-92-layer-1"
               data-x="['center','center','center','center']" data-hoffset="['-5','-5','-5','-5']"
               data-y="['middle','middle','middle','middle']" data-voffset="['-77','-77','-77','-85']"
               data-fontsize="['30','30','25','20']"
               data-fontweight="['600','600','600','500']"
               data-letterspacing="['4','0','0','2']"
               data-width="none"
               data-height="none"
               data-whitespace="nowrap"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":240,"speed":900,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['inherit','inherit','inherit','inherit']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 5; white-space: nowrap; font-size: 30px; line-height: 35px; font-weight: 600; color: #ffffff; letter-spacing: 0; font-family: Poppins, sans-serif;">
            Our Vision
          </div>

          <!-- LAYER NR. 9 -->
          <div class="tp-caption tp-resizeme"
               data-x="['center','center','center','center']" data-hoffset="['0','0','0','3']"
               data-y="['middle','middle','middle','middle']" data-voffset="['0','7','7','-32']"
               data-fontsize="['80','70','60','40']"
               data-width="none"
               data-height="none"
               data-whitespace="nowrap"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":950,"speed":2000,"sfxcolor":"#ffffff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['inherit','inherit','inherit','inherit']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 6; white-space: nowrap; font-size: 70px; line-height: 80px; font-weight: 600; color: #ffffff; letter-spacing: 4px; font-family: Poppins, sans-serif;">
            Making HR Simple
          </div>

          <!-- LAYER NR. 11 -->
          <div class="tp-caption tp-resizeme"
               data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
               data-y="['middle','middle','middle','middle']" data-voffset="['170','170','170','95']"
               data-fontsize="['20','20','16','16']"
               data-width="['601','530','601','450']"
               data-height="none"
               data-whitespace="nowrap"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":2300,"speed":900,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['center','center','center','center']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 9; letter-spacing: .5px;"><a
              class="btn btn-large btn-rounded btn-transparent-white scroll" href="#about">Learn More</a></div>

        </li>

        <!-- SLIDE  -->
        <li data-index="rs-3" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0"
            data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
            data-thumb="images/slide-img2.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide"
            data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7=""
            data-param8="" data-param9="" data-param10="" data-description="">

          <!-- MAIN IMAGE -->
          <img src="<?php echo base_url();?>assets/images/slider-image3.jpg"
               alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off"
               class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->

          <!-- Overlay -->
          <div class="gradient-bg1 bg-overlay opacity-9 z-index-1"></div>

          <!-- LAYER NR. 2 -->
          <div class="tp-caption   tp-resizeme"
               data-x="['right','right','center','center']" data-hoffset="['0','50','0','0']"
               data-y="['top','top','top','top']" data-voffset="['357','309','383','255']"
               data-fontsize="['40','40','32','24']"
               data-width="none"
               data-height="none"
               data-whitespace="nowrap"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":720,"split":"chars","splitdelay":0.05,"speed":900,"split_direction":"forward","frame":"0","from":"sX:0.8;sY:0.8;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['right','right','center','center']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 6; white-space: nowrap; font-size: 40px; line-height: 50px; font-weight: 500; color: #ffffff; letter-spacing: 0; font-family: Poppins, sans-serif;">
            We have pioneered a number of<br>innovative HR Technology Solutions<br>in Indonesia and are now on our<br>journey
            of overseas expansion.
          </div>

          <!-- LAYER NR. 12 -->
          <div class="tp-caption   tp-resizeme"
               data-x="['right','right','center','center']" data-hoffset="['0','50','0','0']"
               data-y="['top','top','middle','middle']" data-voffset="['586','560','164','145']"
               data-width="['601','530','601','450']"
               data-height="none"
               data-whitespace="nowrap"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":2150,"speed":900,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['right','right','center','center']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 9; letter-spacing: .5px; margin-top: 10px;">
            <a class="btn btn-large btn-rounded btn-white scroll" href="#about">Learn More</a>
          </div>
        </li>
      </ul>
    </div>
    <!-- END REVOLUTION SLIDER -->
  </div>

  <!--slider social-->
  <div class="slider-social">
    <ul class="list-unstyled">
      <?php 
          if(!empty($facebook)){
       ?>
      <li class="animated-wrap">
        <a class="animated-element" target="_blank" href="<?php echo $facebook; ?>">
          <i class="fa fa-facebook" aria-hidden="true"></i>
        </a>
      </li>
      <?php 
          }

          if(!empty($twitter)){
       ?>
      <li class="animated-wrap">
        <a class="animated-element" target="_blank" href="<?php echo $twitter; ?>">
          <i class="fa fa-twitter" aria-hidden="true"></i>
        </a>
      </li>
      <?php 
          }

          if(!empty($linkedin)){
       ?>
      <li class="animated-wrap">
        <a class="animated-element" target="_blank" href="<?php echo $linkedin; ?>">
          <i class="fa fa-linkedin" aria-hidden="true"></i>
        </a>
      </li>
      <?php 
          }

          if(!empty($youtube)){
       ?>
      <li class="animated-wrap">
        <a class="animated-element" target="_blank" href="<?php echo $youtube; ?>">
          <i class="fa fa-youtube-play" aria-hidden="true"></i>
        </a>
      </li>
      <?php 
          }

          if(!empty($instagram)){
       ?>
      <li class="animated-wrap">
        <a class="animated-element" target="_blank" href="<?php echo $instagram; ?>">
          <i class="fa fa-instagram" aria-hidden="true"></i>
        </a>
      </li>
      <?php 
          }
       ?>
    </ul>
  </div>

</section>
<!--slider end-->

<!--About Start-->
<section class="p-0">
  <div class="container">

    <!--Slider Image-->
    <div class="row laptop text-center">
      <div class="col-md-12">
        <div class="title d-inline-block laptop-title">
          <h2 class="gradient-text1 mb-3">Shine a light on your HR technology needs today !</h2>
        </div>
        <div class="laptop-img wow fadeInUp">
          <img src="<?php echo base_url();?>assets/images/laptop-img.png" alt="laptop">
          <div id="laptop-slide" class="owl-carousel owl-theme">
            <div class="item">
              <img src="<?php echo base_url();?>assets/images/smart_800x500.png" alt="image">
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
<!--About End-->

<!--Testimonial Start-->
<section id="about">
  <div class="container-fluid">
    <div class="row">
      <!--testimonial-->
      <div class="col-xs-12 col-md-6 bg-light">
        <div id="testimonial_slider" class="owl-carousel">
          <!--testimonial item-->
          <div class="testimonial-item item">
            <i class="fa fa-quote-right testimonial-icon gradient-text1"></i>
            <p class="mb-4">
              Our values acts as our foundation that guides our vision, services and how we measure our success.
              Simply put, it’s how we run our company... by our culture.
            </p>

            <!--Image-->
            <div class="testimonial-image">
              <img src="<?php echo base_url();?>assets/images/Betty%20pic.jpg" alt="image">
            </div>
            <h5 class="font-weight-500 third-color">Betty Mintargo</h5>
            <span class="destination">Co-Owner</span>
          </div>
          <!--testimonial item-->
          <div class="testimonial-item item">
            <i class="fa fa-quote-right testimonial-icon gradient-text1"></i>
            <p class="mb-4">
              We are a company that never accepts people saying "You must do things this way because that it how it has
              always been done."
              We love to innovate. We love to push ourselves to do our best. We love our ASI Asia Pacific family.
            </p>

            <!--Image-->
            <div class="testimonial-image">
              <img src="<?php echo base_url();?>assets/images/pak%20kevin.png" alt="image">
            </div>
            <h5 class="font-weight-500 third-color">Kevin Thompson</h5>
            <span class="destination">Co-Owner</span>
          </div>
        </div>
      </div>
      <!--counters-->
      <div class="col-xs-12 col-md-6 p-0">
        <!--counter background-->
        <div class="counters d-flex align-items-center text-left bg-img1">
          <!--overlay-->
          <div class="bg-overlay gradient-bg1 opacity-8"></div>
          <div class="counter-row asi-values">
            <h3><span class="gradient-bg1">A</span>Act like an Owner</h3>
            <div>
              <p>We believe in doing things today instead of waiting until tomorrow. We make tough decisions when
                necessary. We treat the Company’s assets as our own.</p>
            </div>
            <h3><span class="gradient-bg1">B</span>Be a Team Player</h3>
            <div>
              <p>We respect each other. We listen to each other. We need each other. We are the ASI family.</p>
            </div>
            <h3><span class="gradient-bg1">C</span>Continually Improve</h3>
            <div>
              <p>We learn from our mistakes. We make good ideas even better.</p>
            </div>
            <h3><span class="gradient-bg1">D</span>Deliver Results</h3>
            <div>
              <p>We don’t just say it, we do it. We make things happen but we don’t take short cuts.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Testimonial End-->

<!--Work Start-->
<section id="work" class="cube-portfolio1">
  <div class="container">
    <!--About-->
    <div class="row align-items-center">
      <div class="col-md-6 wow fadeInLeft">
        <div class="title">
          <h2>Our Awesome Solutions</h2>
        </div>
      </div>
      <div class="col-md-6 mb-4 wow fadeInRight">
        <p>
          As a culture-driven HR Tech consultancy, we love to inspire leading organisations and move them to innovate.
          We directly contribute to our clients by helping them build new processes. It all starts with our family of
          superheroes and their Never Give Up attitude.
          <br><br>
          As we all know, there is rarely two or more clients who have the exact same need.
          That's why we continue to work so hard to assist as many companies as possible.
        </p>
      </div>
    </div>

    <div class="row wow fadeInUp">
      <div class="col-md-12">
        <!--Portfolio Items-->
        <div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat">
          <!-- Smart Platform -->
          <div class="cbp-item">
            <a target="_blank" href="http://52.163.84.27/webs/newsmart" class="cbp-caption">
              <div class="cbp-caption-defaultWrap">
                <img src="<?php echo base_url();?>assets/images/smart_600x600.png" alt="port-4">
              </div>
              <div class="cbp-caption-activeWrap"></div>
              <div class="cbp-l-caption-alignCenter center-block">
                <div class="cbp-l-caption-body">
                  <div class="plus"></div>
                  <h5 class="text-white mb-1 bpo-title">Welcome to Asia's #1<br>Online Assessment Platform</h5>
                  <p class="text-white">View More</p>
                </div>
              </div>
            </a>
          </div>
          <!-- VR4HR -->
          <div class="cbp-item">
            <a target="_blank" href="http://52.163.84.27/webs/vr4hr" class="cbp-caption">
              <div class="cbp-caption-defaultWrap">
                <img src="<?php echo base_url();?>assets/images/vr4hr_600x600.png" alt="port-4">
              </div>
              <div class="cbp-caption-activeWrap"></div>
              <div class="cbp-l-caption-alignCenter center-block">
                <div class="cbp-l-caption-body">
                  <div class="plus"></div>
                  <h5 class="text-white mb-1">VR4HR</h5>
                  <p class="text-white">View More</p>
                </div>
              </div>
            </a>
          </div>
          <!-- ONE4ONE -->
          <div class="cbp-item">
            <a target="_blank" href="http://52.163.84.27/webs/one4one" class="cbp-caption">
              <div class="cbp-caption-defaultWrap">
                <img src="<?php echo base_url();?>assets/images/one4one_600x300.png" alt="port-6">
              </div>
              <div class="cbp-caption-activeWrap"></div>
              <div class="cbp-l-caption-alignCenter center-block">
                <div class="cbp-l-caption-body">
                  <div class="plus"></div>
                  <h5 class="text-white mb-1 bpo-title">Program One 4 One<br>adalah sistem CSR yang unik</h5>
                  <p class="text-white">View More</p>
                </div>
              </div>
            </a>
          </div>
          <!-- BPO -->
          <div class="cbp-item" style="cursor: pointer;">
            <a class="cbp-caption" data-toggle="modal" data-target="#bpo_modal">
              <div class="cbp-caption-defaultWrap">
                <img src="<?php echo base_url();?>assets/images/bpo_600x300.png" alt="port-5">
              </div>
              <div class="cbp-caption-activeWrap"></div>
              <div class="cbp-l-caption-alignCenter center-block">
                <div class="cbp-l-caption-body">
                  <div class="plus"></div>
                  <h5 class="text-white mb-1 bpo-title">
                    Business Process Outsourcing<br>(Assessment Centre, Mass Recruitment)
                  </h5>
                  <p class="text-white">View More</p>
                </div>
              </div>
            </a>
          </div>
          <!-- PEEPL -->
          <!--div class="cbp-item">
            <a target="_blank" href="http://52.163.84.27/webs/peepl" class="cbp-caption">
              <div class="cbp-caption-defaultWrap">
                <img src="<?php //echo base_url();?>assets/images/peepl_600x300.png" alt="port-5">
              </div>
              <div class="cbp-caption-activeWrap"></div>
              <div class="cbp-l-caption-alignCenter center-block">
                <div class="cbp-l-caption-body">
                  <div class="plus"></div>
                  <h5 class="text-white mb-1">PEEPL</h5>
                  <p class="text-white">View More</p>
                </div>
              </div>
            </a>
          </div-->
          <!-- Training -->
          <!--div class="cbp-item">
            <a target="_blank" href="http://52.163.84.27/webs/training" class="cbp-caption">
              <div class="cbp-caption-defaultWrap">
                <img src="<?php //echo base_url();?>assets/images/training.png" alt="port-7">
              </div>
              <div class="cbp-caption-activeWrap"></div>
              <div class="cbp-l-caption-alignCenter center-block">
                <div class="cbp-l-caption-body">
                  <div class="plus"></div>
                  <h5 class="text-white mb-1">Training</h5>
                  <p class="text-white">View More</p>
                </div>
              </div>
            </a>
          </div-->
          <!-- INIAKU -->
          <!--div class="cbp-item">
            <a target="_blank" href="http://52.163.84.27/webs/iniaku" class="cbp-caption">
              <div class="cbp-caption-defaultWrap">
                <img src="<?php //echo base_url();?>assets/images/iniaku_300x300.png" alt="port-8">
              </div>
              <div class="cbp-caption-activeWrap"></div>
              <div class="cbp-l-caption-alignCenter center-block">
                <div class="cbp-l-caption-body">
                  <div class="plus"></div>
                  <h5 class="text-white mb-1">INIAKU</h5>
                  <p class="text-white">View More</p>
                </div>
              </div>
            </a>
          </div-->
        </div>
      </div>
    </div>
  </div>
</section>
<!--Work end-->

<!-- Modal BPO -->
<div class="modal fade" id="bpo_modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header gradient-bg1">
        <h5 class="modal-title">Business Process Outsourcing</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row bpo_fade_in" id="bpo_card1">
          <!-- SMART Individual -->
          <div class="col-12 col-lg-4">
            <div class="card" id="card-individual">
              <div class="card-body text-center">
                <h1 class="gradient-text1 mb-3">SMART Individual</h1>
                <p>
                  Our fast and efficient, online assessment solution designed to provide comprehensive
                  individual reports for all levels of your organisation.
                </p>
                <a href="<?php echo base_url(); ?>individual" class="btn btn-large btn-gradient btn-rounded mt-4">Learn More</a>
              </div>
              <div class="card-header"></div>
            </div>
          </div>
          <!-- SMART Volume -->
          <div class="col-12 col-lg-4">
            <div class="card" id="card-volume">
              <div class="card-header"></div>
              <div class="card-body text-center">
                <h1 class="gradient-text1 mb-3">SMART Volume</h1>
                <p>
                  Our most economical online solution, specifically designed for recruiting and assessing large numbers
                  of candidates at an attractive cost.
                </p>
                <a href="<?php echo base_url(); ?>volume" class="btn btn-large btn-gradient btn-rounded mt-4">Learn More</a>
              </div>
            </div>
          </div>
          <!-- SMART AC -->
          <div class="col-12 col-lg-4">
            <div class="card" id="card-ac">
              <div class="card-body text-center">
                <h1 class="gradient-text1 mb-3">SMART AC</h1>
                <p>
                  Assessment Centres are delivered in several different ways, depending on what you wish to measure it,
                  how you wish to measure it, and of course, your budget.
                </p>
                <a href="<?php echo base_url(); ?>ac" class="btn btn-large btn-gradient btn-rounded mt-4">Learn More</a>
              </div>
              <div class="card-header"></div>
            </div>
          </div>
        </div>
        <div class="row bpo_out" id="bpo_card2">
          <div class="col-12">
            <div class="row">
              <div class="col-1">
                <h1><i class="fa fa-arrow-circle-left gradient-text1" id="bpo_back" aria-hidden="true"></i></h1>
              </div>
              <div class="col-10 text-center">
                <h1 class="gradient-text1 mb-3">SMART AC</h1>
              </div>
            </div>
          </div>
          <!-- SMART Gold -->
          <div class="col-12 col-lg-4">
            <div class="card" id="card-gold">
              <div class="card-header"></div>
              <div class="card-body text-center">
                <h2 class="gradient-text1 mb-3">SMART Gold</h2>
                <p>
                  Our cutting-edge, online assessment solution especially designed for manager level participants.
                </p>
                <a target="blank" href="<?php echo base_url(); ?>gold" class="btn btn-large btn-gradient btn-rounded mt-4">Learn More</a>
              </div>
            </div>
          </div>
          <!-- SMART Platinum -->
          <div class="col-12 col-lg-4">
            <div class="card" id="card-platinum">
              <div class="card-header"></div>
              <div class="card-body text-center">
                <h2 class="gradient-text1 mb-3">SMART Platinum</h2>
                <p>
                  The best of both worlds – an assessment centre delivered with a combination of online & face to face
                  administration.
                </p>
                <a target="blank" href="<?php echo base_url(); ?>platinum" class="btn btn-large btn-gradient btn-rounded mt-4">Learn More</a>
              </div>
            </div>
          </div>
          <!-- SMART Diamond -->
          <div class="col-12 col-lg-4">
            <div class="card" id="card-diamond">
              <div class="card-header"></div>
              <div class="card-body text-center">
                <h2 class="gradient-text1 mb-3">Smart Diamond</h2>
                <p>
                  Best in class – our assessment centre solution fully administered in our world class facility.
                </p>
                <a target="blank" href="<?php echo base_url(); ?>diamond" class="btn btn-large btn-gradient btn-rounded mt-4">Learn More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal BPO end -->

<?php $this->view('career/front_end');?>

<?php $this->view('contact_form');?>

<!--Address Start-->
<section class="p-0">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12 col-md-6 p-0">
        <div class="half-map address-box title mb-0 bg-img4">
          <!--overlay-->
          <div class="bg-overlay gradient-bg1 opacity-8"></div>
          <div class="address-text text-center text-white position-relative wow fadeInUp">
            <h6 class="mb-3">Contact Us</h6>
            <!--title-->
            <h3 class="mb-4">PT. Aneka Search Indonesia</h3>
            <!--Address-->
            <p class="mb-3 text-white">
              International Financial Centre Level 19,<br>
              Jalan Jendral Sudirman<br>
              Kav. 22-23, Jakarta 12920
            </p>
            <!--Phone-->
            <p class="mb-3 text-white">
              Office Telephone : +6221 2251 3371
            </p>
            <!--Timing-->
            <p class="mb-3 text-white">Mon-Fri: 8am to 5pm</p>
            <!--Social Icon-->
            <div class="address-social">
              <ul class="list-unstyled">
                <?php 
                    if(!empty($facebook)){
                 ?>
                <li>
                  <a target="_blank" class="facebook-text-hvr" href="<?php echo $facebook; ?>">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                  </a>
                </li>
                <?php 
                    }

                    if(!empty($twitter)){
                 ?>
                <li>
                  <a target="_blank" class="twitter-text-hvr" href="<?php echo $twitter; ?>">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                  </a>
                </li>
                <?php 
                    }

                    if(!empty($linkedin)){
                 ?>
                <li>
                  <a target="_blank" class="linkedin-text-hvr" href="<?php echo $linkedin; ?>">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                  </a>
                </li>
                <?php 
                    }

                    if(!empty($youtube)){
                 ?>
                <li>
                  <a target="_blank" class="youtube-text-hvr" href="<?php echo $youtube; ?>">
                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                  </a>
                </li>
                <?php 
                    }

                    if(!empty($instagram)){
                 ?>
                <li>
                  <a target="_blank" class="instagram-text-hvr" href="<?php echo $instagram; ?>">
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                  </a>
                </li>
                <?php 
                    }
                 ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 p-0">
        <iframe class="half-map bg-img-map" style="border: 0;" allowfullscreen
                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1983.1974181853595!2d106.820581!3d-6.2115433!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f403cbfcefcd%3A0xb4e50142ed6b1a9f!2sInternational+Financial+Centre+-+Jakarta!5e0!3m2!1sen!2sid!4v1560511222253!5m2!1sen!2sid"></iframe>
      </div>
    </div>
  </div>
</section>
<!--Address End-->

<!--Footer Start-->
<section class="bg-light text-center p5rem">
  <h2 class="d-none">hidden</h2>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="footer-social">
          <ul class="list-unstyled">
            <?php 
                if(!empty($facebook)){
             ?>
            <li>
              <a target="_blank" class="wow fadeInUp" href="<?php echo $facebook; ?>">
                <i class="fa fa-facebook" aria-hidden="true"></i>
              </a>
            </li>
            <?php 
                }

                if(!empty($twitter)){
             ?>
            <li>
              <a target="_blank" class="wow fadeInDown" href="<?php echo $twitter; ?>">
                <i class="fa fa-twitter" aria-hidden="true"></i>
              </a>
            </li>
            <?php 
                }
                
                if(!empty($linkedin)){
             ?>
            <li>
              <a target="_blank" class="wow fadeInUp" href="<?php echo $linkedin; ?>">
                <i class="fa fa-linkedin" aria-hidden="true"></i>
              </a>
            </li>
            <?php 
                }
                
                if(!empty($youtube)){
             ?>
            <li>
              <a target="_blank" class="wow fadeInDown" href="<?php echo $youtube; ?>">
                <i class="fa fa-youtube-play" aria-hidden="true"></i>
              </a>
            </li>
            <?php 
                }
                
                if(!empty($instagram)){
             ?>
            <li>
              <a target="_blank" class="wow fadeInUp" href="<?php echo $instagram; ?>">
                <i class="fa fa-instagram" aria-hidden="true"></i>
              </a>
            </li>
            <?php 
                }
             ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Footer End-->

<!--Scroll Top-->
<a class="scroll-top-arrow" href="javascript:void(0);"><i class="fa fa-angle-up"></i></a>
<!--Scroll Top End-->

<!--Animated Cursor-->
<div id="aimated-cursor">
  <div id="cursor">
    <div id="cursor-loader"></div>
  </div>
</div>

<div class="modal fade hide-close-btn" id="thankyouModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header gradient-bg1">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h5 class="modal-title" id="myModalLabel">Sending Message</h5>
      </div>
      <div class="modal-body">
        <img id="ajax_loader" src="<?php echo base_url(); ?>assets/images/ajax-loader.gif" style="display: block; margin-left: auto; margin-right: auto;">
        <!--				<p>Thanks for getting in touch!</p>-->
        <div class="notif_success" id="notif_success">
          <i class="fa fa-check"></i>
          <label id="text_success"></label>
        </div>
        <div class="notif_error" id="notif_error">
          <i class="fa fa-times-circle"></i>
          <label id="text_error"></label>
        </div>
      </div>
      <div class="modal-footer">
        <!--				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        <button type="button" class="btn btn-primary btn-gradient btn-rounded" data-dismiss="modal" id="btn_ok">OK</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header gradient-bg1">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>

<!-- Optional JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.appear.js"></script>
<!-- cube portfolio gallery -->
<script src="<?php echo base_url();?>assets/js/jquery.cubeportfolio.min.js"></script>
<!-- owl carousel slider -->
<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
<!-- wow animation -->
<script src="<?php echo base_url();?>assets/js/wow.js"></script>
<!-- tween max animation -->
<script src="<?php echo base_url();?>assets/js/TweenMax.min.js"></script>
<!-- REVOLUTION JS FILES -->
<script src="<?php echo base_url();?>assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url();?>assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION EXTENSIONS -->
<script src="<?php echo base_url();?>assets/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url();?>assets/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url();?>assets/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?php echo base_url();?>assets/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
<!-- custom script -->
<script src="<?php echo base_url();?>assets/js/script.js"></script>
<script src="<?php echo base_url();?>assets/js/custom_front.js"></script>
</body>
</html>