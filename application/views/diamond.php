<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Page Title -->
  <title>ASI Asia Pacific | Making HR Simple</title>
  <!-- Favicon -->
  <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico">
  <!-- Animate -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.min.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
  <!-- Swiper Style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/swiper.min.css">
  <!-- Style Sheet -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="90">

<!--Loader Start-->
<div class="loader">
  <div class="loader-inner">
    <div class="loader-blocks">
      <span class="block-1"></span>
      <span class="block-2"></span>
      <span class="block-3"></span>
      <span class="block-4"></span>
      <span class="block-5"></span>
      <span class="block-6"></span>
      <span class="block-7"></span>
      <span class="block-8"></span>
      <span class="block-9"></span>
      <span class="block-10"></span>
      <span class="block-11"></span>
      <span class="block-12"></span>
      <span class="block-13"></span>
      <span class="block-14"></span>
      <span class="block-15"></span>
      <span class="block-16"></span>
    </div>
  </div>
</div>
<!--Loader End-->

<!--Header Start-->
<header class="cursor-light header-appear">

  <!--Navigation-->
  <nav class="navbar navbar-top-default navbar-expand-lg navbar-gradient nav-icon">
    <div class="container">
      <a href="<?php echo base_url();?>#home" title="Logo" class="logo link">
        <!--Logo Default-->
        <img src="<?php echo base_url();?>assets/images/asi80.png" alt="logo" class="logo-dark default">
      </a>

      <!--Nav Links-->
      <div class="collapse navbar-collapse" id="wexim">
        <div class="navbar-nav ml-auto">
          <a class="nav-link link" href="<?php echo base_url();?>#home">Home</a>
          <a class="nav-link link" href="<?php echo base_url();?>#about">About</a>
          <a class="nav-link link" href="<?php echo base_url();?>#work">Work</a>
          <a class="nav-link link" href="<?php echo base_url();?>#careers">Careers</a>
          <a class="nav-link link" href="<?php echo base_url();?>#contact">Contact</a>
          <span class="menu-line"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
        </div>
      </div>

      <!--Side Menu Button-->
      <a href="javascript:void(0)" class="d-inline-block parallax-btn sidemenu_btn" id="sidemenu_toggle">
        <div class="animated-wrap sidemenu_btn_inner">
        <div class="animated-element">
          <span></span>
          <span></span>
          <span></span>
        </div>
        </div>
      </a>
    </div>
  </nav>

  <!--Side Nav-->
  <div class="side-menu">
    <div class="inner-wrapper">
      <span class="btn-close link" id="btn_sideNavClose"></span>
      <nav class="side-nav w-100">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link link" href="<?php echo base_url();?>#home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link link" href="<?php echo base_url();?>#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link link" href="<?php echo base_url();?>#work">Work</a>
          </li>
          <li class="nav-item">
            <a class="nav-link link" href="<?php echo base_url();?>#careers">Careers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link link" href="<?php echo base_url();?>#contact">Contact</a>
          </li>
        </ul>
      </nav>

      <div class="side-footer text-white w-100">
        <ul class="social-icons-simple">
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="https://www.facebook.com/asiasiapacific">
              <i class="fa fa-facebook"></i>
            </a>
          </li>
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="https://twitter.com/asiasiapacific">
              <i class="fa fa-twitter"></i>
            </a>
          </li>
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="https://id.linkedin.com/company/pt-aneka-search-indonesia">
              <i class="fa fa-linkedin"></i>
            </a>
          </li>
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="https://www.youtube.com/channel/UCxaKd-iUXcLIhHmEyaCXbbA">
              <i class="fa fa-youtube-play"></i>
            </a>
          </li>
          <li class="animated-wrap">
            <a class="animated-element" target="_blank" href="https://www.instagram.com/asiasiapacific/">
              <i class="fa fa-instagram"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <a id="close_side_menu" href="javascript:void(0);"></a>
  <!-- End side menu -->

</header>
<!--Header end-->

<section class="swiper-container">
  <div class="container p-4 wow fadeInUp text-center">
    <h1 class="gradient-text1">SMART Diamond</h1><br>
    <p>Best in class – our assessment centre solution fully administered in our world class facility</p>
  </div>
  <div class="container-fluid advantage swiper-wrapper">
    <!-- 1 -->
    <div class="row swiper-slide">
      <div class="col-12 col-sm-6 testimonial-item wow fadeInLeft">
        <h2>The Answer to your Needs</h2>
        <h6></h6>
        <ul style="list-style-type: disc; margin-left: 30px;">
          <li>Recruitment</li>
          <li>Development</li>
          <li>Promotion</li>
        </ul>
      </div>
      <div class="col-12 col-sm-6 volume-img show-first wow fadeInRight">
        <img src="<?php echo base_url();?>assets/images/ac-1.jpg">
      </div>
    </div>
    <!-- 5 -->
    <div class="row swiper-slide">
      <div class="col-12 col-sm-6 wow fadeInLeft">
        <img src="<?php echo base_url();?>assets/images/ac-5.jpg">
      </div>
      <div class="col-12 col-sm-6 testimonial-item wow fadeInRight">
        <h2>Dedicated Assessors</h2>
        <h6></h6>
        <p>Only highly trained assessors are used to evaluate a participant’s decisions and actions.</p>
      </div>
    </div>
    <!-- 7 -->
    <div class="row swiper-slide">
      <div class="col-12 col-sm-6 testimonial-item wow fadeInLeft">
        <h2>Easy to Read Reports</h2>
        <h6></h6>
      </div>
      <div class="col-12 col-sm-6 volume-img show-first wow fadeInRight">
        <img src="<?php echo base_url();?>assets/images/ac-7.jpg">
      </div>
    </div>
  </div>
  <div class="swiper-pagination"></div>
  <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div>
</section>

<!--Footer Start-->
<section class="bg-light text-center">
  <h2 class="d-none">hidden</h2>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="footer-social">
          <ul class="list-unstyled">
            <li><a target="_blank" class="wow fadeInUp" href="https://www.facebook.com/asiasiapacific"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a target="_blank" class="wow fadeInDown" href="https://twitter.com/asiasiapacific"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a target="_blank" class="wow fadeInUp" href="https://id.linkedin.com/company/pt-aneka-search-indonesia"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            <li><a target="_blank" class="wow fadeInDown" href="https://www.youtube.com/channel/UCxaKd-iUXcLIhHmEyaCXbbA"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
            <li><a target="_blank" class="wow fadeInUp" href="https://www.instagram.com/asiasiapacific/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Footer End-->

<!--Scroll Top-->
<a class="scroll-top-arrow" href="javascript:void(0);"><i class="fa fa-angle-up"></i></a>
<!--Scroll Top End-->

<!--Animated Cursor-->
<div id="aimated-cursor">
  <div id="cursor">
    <div id="cursor-loader"></div>
  </div>
</div>

<!-- Optional JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.appear.js"></script>
<!-- tween max animation -->
<script src="<?php echo base_url();?>assets/js/TweenMax.min.js"></script>
<!-- Swiper js -->
<script src="<?php echo base_url();?>assets/js/swiper.min.js"></script>
<!-- custom script -->
<script src="<?php echo base_url();?>assets/js/script-detail.js"></script>
</body>
</html>