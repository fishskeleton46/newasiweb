<?php
    $active_menu = (isset($active_menu)) ? $active_menu : '';
    $sub_active_menu = (isset($sub_active_menu)) ? $sub_active_menu : '';

    $career_active = ($active_menu == 'career') ? 'active' : '';
    $contact_active = ($active_menu == 'contact') ? 'active' : '';

    $contact_sub_active = ($sub_active_menu == 'contact') ? 'active' : '';
    $apply_job_sub_active = ($sub_active_menu == 'apply_job') ? 'active' : '';
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo site_url('/') ?>" target="blank">
    <div class="sidebar-brand-icon my-2">
      <img src="<?php echo base_url();?>assets/images/asi80.png" alt="">
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <li class="nav-item <?php echo $career_active;?>">
    <a class="nav-link" href="<?php echo site_url('career') ?>">
      <i class="fas fa-users"></i>
      <span>Career</span></a>
  </li>

  <li class="nav-item <?php echo $contact_active;?>">
      <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-headset"></i>
        <span>Contact</span>
      </a>
      <div id="collapsePages" class="collapse <?php echo ($contact_active == 'active') ? 'show' : '';?>" aria-labelledby="headingPages" data-parent="#accordionSidebar" style="">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item <?php echo $contact_sub_active; ?>" href="<?php echo site_url('contact') ?>">Contact</a>
          <a class="collapse-item <?php echo $apply_job_sub_active; ?>" href="<?php echo site_url('contact/apply_job') ?>">Apply Job</a>
        </div>
      </div>
    </li>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->