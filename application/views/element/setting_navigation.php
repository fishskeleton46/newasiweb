<?php 
		$general = ($active_tab == 'general') ? 'active' : '';
		$password = ($active_tab == 'password') ? 'active' : '';
		$social_media = ($active_tab == 'social_media') ? 'active' : '';
 ?>
<ul class="nav nav-tabs without-border-bottom" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link <?php echo $general; ?>" href="<?php echo site_url('access/general');?>">General</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php echo $password; ?>" href="<?php echo site_url('access/security');?>">Password</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php echo $social_media; ?>" href="<?php echo site_url('access/social_media');?>">Social Media</a>
  </li>
</ul>