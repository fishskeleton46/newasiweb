<!--Contact Start-->
<section id="contact">
  <div class="container">
    <!--Heading-->
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="title d-inline-block">
          <h2 class="gradient-text1">Reach Us Quickly</h2>
        </div>
      </div>
    </div>

    <!--contact us-->
    <form action="<?php echo base_url(); ?>contact_submit" class="contact-form" method="post" id="contactForm">
      <div class="row">

        <div class="col-sm-12" id="result">
	        <?php echo validation_errors(); ?>
        </div>

        <div class="col-md-6 col-sm-6">
          <div class="form-group">
            <input class="form-control" type="text" id="name" name="name" placeholder="Your Name" required>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="form-group">
            <input class="form-control" type="text" id="job-title" name="job-title" placeholder="Your Job Title"
                   required>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="form-group">
            <input class="form-control" type="text" id="company" name="company" placeholder="Your Company" required>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="form-group">
            <select name="no-of-staff" class="form-control" id="no-of-staff" required>
              <option disabled selected value="">Number of Employees</option>
              <option value="1 - 100">1 - 100</option>
              <option value="101- 500">101 - 500</option>
              <option value="501 - 1000">501 - 1000</option>
              <option value="1001 - 5000">1001 - 5000</option>
              <option value="5001 - 10000">5001 - 10000</option>
              <option value="10000+">10000+</option>
            </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="form-group">
            <input class="form-control" type="number" id="phone" name="phone" placeholder="Your Mobile Phone" required>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="form-group">
            <input class="form-control" type="email" id="email" name="email" placeholder="Your email" required>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
            <textarea class="form-control" placeholder="Message" id="message" name="message" required></textarea>
          </div>
        </div>
        <div class="col-sm-12">
	        <?php echo $this->recaptcha->render(); ?>
        </div>
        <div class="col-sm-12">
          <button type="submit" class="btn btn-large btn-gradient btn-rounded mt-4" id="btn_contact_us">
            <i class="fa fa-spinner fa-spin mr-2 d-none" aria-hidden="true"></i> <span>Send Message</span>
          </button>
        </div>
      </div>
    </form>
  </div>
</section>
<!--Contact End-->