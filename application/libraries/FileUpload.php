<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
	class FileUpload
	{
		private $CI;

		public $allowed_types = 'gif|jpg|png';
		public $max_size = 1024; // 1 MegaByte

		public $root_folder = './assets/uploads/';

		public function __construct()  
		{ 
			$this->CI =& get_instance(); 
		}

		private function _makeDir($path)
		{
			$path = explode('/', $path);

			$temp_path = '';
			foreach ($path as $key => $value) {
				$temp_path .= $value.'/';

				if(!file_exists($temp_path)){
					mkdir($temp_path);
				}
			}
		}

		public function _uploadImage($name, $folder)
		{

			$folder = $this->root_folder.$folder;

			$this->_makeDir($folder);

		    $config['upload_path']          = $folder;
		    $config['allowed_types']        = $this->allowed_types;
		    // $config['overwrite']			= true;
		    $config['max_size']             = $this->max_size; // 1MB

		    $this->CI->load->library('upload', $config);

		    if ($this->CI->upload->do_upload($name)) {
		    	$result = array(
		    		'status' 	=> 'success',
		    		'file_name' => $this->CI->upload->data("file_name"),
		    		'message' 	=> ''
		    	);
		    }else{
		    	$result = array(
		    		'status' 	=> 'error',
		    		'file_name' => '',
		    		'message' 	=> strip_tags($this->CI->upload->display_errors())
		    	);
		    }
		    
		    return $result;
		}

		public function _deleteImage($path)
		{
			$this->CI->load->helper("file");
			delete_files($path);
		}
	}
 ?>