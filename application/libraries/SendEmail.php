<?php 
	/**
	 * 
	 */
	class SendEmail
	{
		private $CI;

		public $from = 'invitation@asi-asiapacific.com';
		public $from_text = 'Admin SMART Platform';

		public function __construct()  
		{ 
			$this->CI =& get_instance(); 
		}

		public function send($to, $subject, $template, $data)
		{
			$body_message = $this->CI->load->view('email/'.$template, array(
				'data' => $data
			), true);

			$body_email_real = $this->CI->load->view('layouts/email', array(
				'content_for_layout' => $body_message
			), true);

			$config = array(
				"protocol"	=> "smtp",
				"smtp_host"	=> "ssl://email-smtp.us-east-1.amazonaws.com",
				"smtp_user"	=> "AKIAIBO4EU2SY7U4WDEA",
				"smtp_pass"	=> "AhQSWDcaCn5Tvi6MQ047LnmlgSOUE6zIwiDagixSce//",
				"smtp_port"	=> 465,
				"mailtype"	=> "html",
				"charset"		=> "utf-8",
				"newline"		=> "\r\n",
				"wordwrap"	=> true
			);

			$this->CI->email->initialize($config);
			$this->CI->email->set_newline("\r\n");
			$this->CI->email->from($this->from, $this->from_text);

			$real_to_email = '';
			$other_to_email = array();
			if(is_array($to)){
				$count = count($to);
				if($count > 1){
					$idx = 1;
					foreach ($to as $key => $value) {
						if($idx == 1){
							$real_to_email = $value;
							$idx++;
						}else{
							$other_to_email[] = $value;
						}
					}
				}else{
					$real_to_email = $to[0];
				}
			}else{
				$real_to_email = $to;
			}

			$this->CI->email->to($real_to_email);
			if(!empty($other_to_email)){
				$this->CI->email->cc($other_to_email);
			}
			
			$this->CI->email->subject($subject);
			$this->CI->email->message($body_email_real);
			$sending = $this->CI->email->send();

			if( !$sending ) {
				$message	= show_error($this->CI->email->print_debugger());
				$status 	= 'error';
			} else {
				$message	= "Message send successfully.";
				$status 	= 'success';
			}

			return array(
				'status' => $status,
				'message' => $message,
			);
		}
	}
 ?>