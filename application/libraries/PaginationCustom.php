<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
/** 
 * PaginationConfig Class. PHP5 only. 
 * 
 */
class PaginationCustom { 
  
  private $CI;

  public $from_segment;

  public $config = array(
    'reuse_query_string' => true,
    'per_page' => 10
  );

  public $style = array(
    'query_string_segment' => 'start',
    'full_tag_open' => '<nav aria-label="Page navigation example"><ul class="pagination float-right">',
    'full_tag_close' => '</ul></nav>',
    'first_link' => 'First',
    'first_tag_open' => '<li class="page-item">',
    'first_tag_close' => '</li>',
    'last_link' => 'Last',
    'last_tag_open' => '<li class="page-item">',
    'last_tag_close' => '</li>',
    'next_link' => 'Next',
    'next_tag_open' => '<li class="page-item">',
    'next_tag_close' => '</li>',
    'prev_link' => 'Prev',
    'prev_tag_open' => '<li class="page-item">',
    'prev_tag_close' => '</li>',
    'cur_tag_open' => '<li class="page-item active" aria-current="page"><a href="javascript:void(0)" class="page-link">',
    'cur_tag_close' => ' <span class="sr-only">(current)</span></a></li>',
    'num_tag_open' => '<li class="page-item">',
    'num_tag_close' => '</li>',
    '_attributes' => 'class="page-link"'
  );

  public function __construct()  
  { 
    $this->CI =& get_instance();

  }

  public function initialize($base_url, $total_rows)
  {
    $this->CI->load->library('pagination');

    $this->total_rows         = $total_rows;
    $this->config['base_url'] = $base_url;
    $this->config['total_rows'] = $total_rows;
    $this->from_segment       = $this->CI->uri->segment(3);

    $config = array_merge($this->config, $this->style);

    $this->CI->pagination->initialize($config);
  }

}