<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller
{
	function __construct(){
		parent::__construct();		
	}

	public function login(){
		if( $this->session->userdata("status") == "login" ) redirect( base_url('home') );

		$this->load->library('Layouts');
		$this->load->model('user');

		$Layouts = new Layouts();

		$data = array(
			'username' => '',
		);

		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$username     = $this->input->post("username");
			$password     = $this->input->post("password");

			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if ($this->form_validation->run() == TRUE){
				$result = $this->user->check_login($username, md5($password));

				if(!empty($result)){
					$data_session = array(
						'id' => $result->id,
						'nama' => $username,
						'status' => "login"
					);

					$this->session->set_userdata($data_session);

					redirect( base_url('home') );
				}else{

				}
            }
		}

		$Layouts->set_title('Login');
		$Layouts->view('login', $data, 'login');
	}

	public function logout(){
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

	public function general()
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$this->load->library('Layouts');
		$this->load->library('session');
		$this->load->model('m_setting');

		$data = $this->m_setting->getData(array('email-contact'));

		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$setting = $this->input->post("setting");

			$result = $this->m_setting->general_setting($setting);

			$this->session->set_flashdata('success', 'successfully update general setting');
			redirect( base_url('access/general') );
		}

		$Layouts = new Layouts();
		$Layouts->set_title('Setting');
		$Layouts->view('setting/general', array(
			'active_tab' => 'general',
			'data' => $data
		));
	}

	public function social_media()
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$this->load->library('Layouts');
		$this->load->library('session');
		$this->load->model('m_setting');

		$data = $this->m_setting->getData(array('facebook', 'twitter', 'instagram', 'youtube', 'linkedin'));

		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$setting = $this->input->post("setting");

			$result = $this->m_setting->general_setting($setting);

			$this->session->set_flashdata('success', 'successfully update social media');
			
			redirect( base_url('access/social_media') );
		}

		$Layouts = new Layouts();
		$Layouts->set_title('Setting');
		$Layouts->view('setting/social_media', array(
			'active_tab' => 'social_media',
			'data' => $data
		));
	}

	public function security()
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$this->load->library('Layouts');
		$this->load->library('session');
		$this->load->model('user');

		$id = $this->session->userdata("id");

		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$old_password     	= $this->input->post("old_password");
			$new_password     	= $this->input->post("new_password");
			$confirm_password   = $this->input->post("confirm_password");

			$this->form_validation->set_rules('old_password', 'Old Password', 'required');
			$this->form_validation->set_rules('new_password', 'New Password', 'required');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[new_password]');

			if ($this->form_validation->run() == TRUE){
				$result = $this->user->check_password($id, md5($old_password));

				if(!empty($result)){
					$result = $this->user->update_password($id, $new_password);

					if(!empty($result)){
						$this->session->set_flashdata('success', 'successfully update password');
						redirect( base_url('access/security') );
					}else{
						$this->session->set_flashdata('error', 'Failed to change your password, please call the administrator');	
					}
				}else{
					$this->session->set_flashdata('error', 'Your old password not match');
				}
			}
		}

		$Layouts = new Layouts();
		$Layouts->set_title('Setting');
		$Layouts->view('setting/setting', array(
			'active_tab' => 'password'
		));
	}
}