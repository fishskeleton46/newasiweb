<?php 
	class Contact extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();

			if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

			$this->load->model('m_contact');
		}

		public function search($action)
		{
			$data = $_POST;
			$keyword = array();
			foreach ($data as $key => $value) {
				if(!empty($value)){
					$keyword[] = $key.'='.$value;
				}
			}

			if(!empty($keyword)){
				$keyword = '?'.implode('&', $keyword);
			}else{
				$keyword = '';
			}

			redirect(base_url(str_replace('-', '/', $action).$keyword));
		}

		public function index()
		{
			$this->load->helper(array('url'));

			$this->load->library('Layouts');
			$this->load->library('PaginationCustom');

			$keyword = $this->input->get('keyword');

			$this->m_contact->setCondition($keyword);

			$jumlah_data = $this->m_contact->jumlah_data();

			$paginate = new PaginationCustom();
			$paginate->initialize(base_url().'contact/index/', $jumlah_data);

			$contact = $this->m_contact->data();

			$Layouts = new Layouts();
			$Layouts->set_title('Contact');
			$Layouts->view('contact/list', array(
				'contact' => $contact,
				'keyword' => $keyword,
			), 'admin', 'contact', 'contact');
		}

		public function detail($id)
		{
			$contact = $this->m_contact->getData($id);

			if(empty($contact)){
				$this->session->set_flashdata('error', 'Data not found');

				redirect( base_url('contact') );
			}else{
				$this->m_contact->hasRead($id);

				$this->load->library('Layouts');
			}

			$Layouts = new Layouts();
			
			$Layouts->set_title('Detail Contact');
			$Layouts->view('contact/detail', array(
				'id' 		=> $id,
				'contact' 	=> $contact,
			), 'admin', 'contact', 'contact');
		}

		public function apply_job()
		{
			$this->load->model('m_apply_job');
			$this->load->helper(array('url'));

			$this->load->library('Layouts');
			$this->load->library('PaginationCustom');

			$keyword = $this->input->get('keyword');

			$this->m_apply_job->setCondition($keyword);

			$jumlah_data = $this->m_apply_job->jumlah_data();

			$paginate = new PaginationCustom();
			$paginate->initialize(base_url().'contact/apply_job/', $jumlah_data);

			$apply_job = $this->m_apply_job->data($paginate->config['per_page'], $paginate->from_segment);

			if(!empty($apply_job)){
				$this->load->model('m_career');
				foreach ($apply_job as $key => $value) {
					$ref =& $apply_job[$key];

					$id_career = $value->id_career;

					$ref->career = $this->m_career->getData($id_career);
				}
			}

			$Layouts = new Layouts();
			$Layouts->set_title('Apply Job');
			$Layouts->view('contact/apply_job_list', array(
				'apply_job' => $apply_job,
				'keyword' => $keyword,
			), 'admin', 'contact', 'apply_job');
		}
	}
 ?>