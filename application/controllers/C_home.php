<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_home extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->model(array('m_career', 'm_setting'));

		$setting = $this->m_setting->getData(array('facebook', 'twitter', 'instagram', 'youtube', 'linkedin'));
		$career = $this->m_career->getAllData();

		$this->load->view('index', array(
			'career' => $career,
			'setting' => $setting
		));
	}

	public function individual(){
		$this->load->view('individual');
	}

	public function volume(){
		$this->load->view('volume');
	}

	public function ac(){
		$this->load->view('ac');
	}

	public function contactSubmit(){
		try{
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('job-title', 'Job Title', 'trim|required');
			$this->form_validation->set_rules('company', 'Company', 'trim|required');
			$this->form_validation->set_rules('no-of-staff', 'Number of Employees', 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('message', 'Message', 'trim|required');

			if($this->form_validation->run() === FALSE){
				$response = array(
					'status' => 'error',
					'message' => validation_errors()
				);

			} else {
				$captcha_answer = $this->input->post('g-recaptcha-response');
				$response = $this->recaptcha->verifyResponse($captcha_answer);

				if ($response['success']){

					$email = $this->input->post('email', true);
					$stat_verify_email = $this->emailValidation($email);

					if($stat_verify_email['status'] == '1'){
						$contact_data = array(
							'name' 				=> $this->input->post('name', true),
							'job_title'		=> $this->input->post('job-title', true),
							'company' 		=> $this->input->post('company', true),
							'no_of_staff'	=> $this->input->post('no-of-staff', true),
							'phone' 		=> $this->input->post('phone', true),
							'email' 		=> $email,
							'message' 		=> $this->input->post('message', true),
							'created_at' 	=> date('Y-m-d H:i:s'),
						);

						$this->load->model('m_contact');
						$id = $this->m_contact->insert($contact_data);
						$message = $this->sendMail($contact_data);

						$response = array(
							'status' => 'success',
							'message' => $message
						);
					} else {
						$response = array(
							'status' => 'error',
							'message' => $stat_verify_email['message']
						);
					}
				} else {
					$response = array(
						'status' => 'error',
						'message' => 'Captcha Invalid'
					);
				}

			}

		} catch (Exception $e){
			$response = array(
				'status' => 'error',
				'message' => 'Exception : '. $e->getMessage() ."\n"
			);
		}

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));

	}

	protected function sendMail($data){
		$this->load->model(array('m_setting'));

		$setting = $this->m_setting->getData(array('email-contact'));

		$config = array(
			"protocol"	=> "smtp",
			"smtp_host"	=> "ssl://email-smtp.us-east-1.amazonaws.com",
			"smtp_user"	=> "AKIAIBO4EU2SY7U4WDEA",
			"smtp_pass"	=> "AhQSWDcaCn5Tvi6MQ047LnmlgSOUE6zIwiDagixSce//",
			"smtp_port"	=> 465,
			"mailtype"	=> "html",
			"charset"		=> "utf-8",
			"newline"		=> "\r\n",
			"wordwrap"	=> true
		);

		$to = 'yohanes@asi-asiapacific.com';
		if(!empty($setting['email-contact'])){
			$to = $setting['email-contact'];
		}

		$body_message = '<table>';
		$body_message .= '<tr><td>Name </td><td> : </td><td><span style="font-weight:bold"> ' . $data['name'] . '</span></td></tr>';
		$body_message .= '<tr><td>Job Title </td><td> : </td><td><span style="font-weight:bold">' . $data['job-title'] . '</span></td></tr>';
		$body_message .= '<tr><td>Company </td><td> : </td><td><span style="font-weight:bold">' . $data['company'] . '</span></td></tr>';
		$body_message .= '<tr><td>Number of Employees </td><td> : </td><td><span style="font-weight:bold"> ' . $data['no-of-staff'] . '</span></td></tr>';
		$body_message .= '<tr><td>Phone </td><td> : </td><td><span style="font-weight:bold">' . $data['phone'] . '</span></td></tr>';
		$body_message .= '<tr><td>Email </td><td> : </td><td><span style="font-weight:bold">' . $data['email'] . '</span></td></tr>';
		$body_message .= '<tr><td>Message </td><td> : </td><td><span style="font-weight:bold">' . $data['message'] . '</span></td></tr>';
		$body_message .= '</table>';

		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from("invitation@asi-asiapacific.com", "Admin SMART Platform");
		$this->email->to($to);
		//$this->email->to("kevin@asi-asiapacific.com");
		//$this->email->bcc("cahyadi@asi-asiapacific.com");
		//$this->email->bcc("softdev.backup@gmail.com");
		$this->email->reply_to($data['email']);
		$this->email->subject("ASI website Inquiry");
		$this->email->message($body_message);
		$sending = $this->email->send();

		if( !$sending ) {
			$message	= show_error($this->email->print_debugger());
		} else {
			$message	= "Message send successfully.";
		}
		return $message;
	}

	protected function verifyEmail($email){
		$this->load->library('VerifyEmail');
		$mail = new VerifyEmail();
		$mail->setStreamTimeoutWait(10);
		$mail->Debug = FALSE;
		//$mail->Debugoutput= 'html';

		$mail->setEmailFrom('cahyadi1@asi-asiapacific.com');
		//$email = 'cahyadi1@asi-asiapacific.com';

		// Check if email is valid and exist
		if($mail->check($email)){
			$status = '1';
			$message = 'Email is exist';
		} elseif(verifyEmail::validate($email)){
			$status = '2';
			$message = 'Email is valid, but not exist!';
		} else{
			$status = '3';
			$message = 'Email is not valid and not exist';
		}
		$result = array(
			'status' => $status,
			'message' => $message
		);
		return $result;
	}

	protected function emailValidation($email){
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		$continue = TRUE;
		$mail = explode("@", $email);
		if (count($mail) != 2) {
			$continue = FALSE;
		}
		if ($continue){
			$mailDomain = $mail[1];
			$validDomain = checkdnsrr($mailDomain, "MX");
			if (filter_var($email, FILTER_VALIDATE_EMAIL) && $validDomain) {
				$status = '1';
				$message = 'Email address is valid';
			} else if (filter_var($email, FILTER_VALIDATE_EMAIL) && !$validDomain) {
				$status = '2';
				$message = 'Email domain does not exist';
			}	else {
				$status = '3';
				$message = 'Email address is not valid';
			}
		} else {
			$status = '4';
			$message = 'Email address is not valid, domain not found';
		}
		$result = array(
			'status' => $status,
			'message' => $message
		);
		return $result;
	}

}
