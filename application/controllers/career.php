<?php 
class Career extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('m_career');

		$this->load->library('session');
	}

	public function search($action)
		{
			$data = $_POST;
			$keyword = array();
			foreach ($data as $key => $value) {
				if(!empty($value)){
					$keyword[] = $key.'='.$value;
				}
			}

			if(!empty($keyword)){
				$keyword = '?'.implode('&', $keyword);
			}else{
				$keyword = '';
			}

			redirect(base_url(str_replace('-', '/', $action).$keyword));
		}

	public function index()
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$this->load->helper(array('url'));

		$this->load->library('Layouts');
		$this->load->library('PaginationCustom');

		$keyword = $this->input->get('keyword');

		$this->m_career->setCondition($keyword);

		$jumlah_data = $this->m_career->jumlah_data();

		$paginate = new PaginationCustom();
		$paginate->initialize(base_url().'career/index/', $jumlah_data);

		$career = $this->m_career->data();
		if(!empty($career)){
			foreach ($career as $key => $value) {
				$ref =& $career[$key];

				$ref->information = $this->m_career->data_information($value->id_career);
			}
		}

		$Layouts = new Layouts();
		$Layouts->set_title('Career');
		$Layouts->view('career/list', array(
			'career' => $career,
			'keyword' => $keyword,
		), 'admin', 'career');
	}

	public function add()
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$this->load->library('Layouts');
		$this->load->library('FileUpload');
		$this->load->helper('form');

		$skill_set 		= '';
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$FileUpload = new FileUpload();

			$name_career 	= $this->input->post('name_career');
			$location 		= $this->input->post('location');
			$type_job 		= $this->input->post('type_job');
			$description 	= $this->input->post('description');
			$status 		= $this->input->post('status');
			$skill_name 	= $this->input->post('skill_name');
			$skill_value 	= $this->input->post('skill_value');

			if(empty($status)){
				$status = 0;
			}

			$this->form_validation->set_rules('name_career', 'Career name', 'required');
			$this->form_validation->set_rules('type_job', 'Type job', 'required');

			$temp_skill_set = array();
			if(!empty($skill_name)){
				foreach ($skill_name as $key => $value) {
					$temp_skill_set[] = array(
						'name' => $value,
						'value' => ($skill_value[$key]) ? $skill_value[$key] : 0
					);
				}
			}

			$skill_set = json_encode($temp_skill_set);

			if ($this->form_validation->run() == TRUE){
				$image = $FileUpload->_uploadImage('image', 'career');
				
				if($image['status'] == 'success'){
					$data = array(
						'name_career' 	=> $name_career,
						'location' 		=> $location,
						'type_job' 		=> $type_job,
						'description' 	=> $description,
						'skill_set' 	=> $skill_set,
						'status' 		=> $status,
						'image' 		=> $image['file_name'],
					);

					$result = $this->m_career->insert($data);
					
					if($result){
						$this->session->set_flashdata('success', 'successfully added data');

						redirect( base_url('career') );
					}else{
						$this->session->set_flashdata('error', 'Failed to add data');
					}
				}else{
					$this->session->set_flashdata('warning', $image['message']);
					$this->session->set_flashdata('error', 'Failed to add data');
				}

			}
		}

		$Layouts = new Layouts();
		$Layouts->set_title('Add Career');
		$Layouts->view('career/form', array(
			'action' 	=> 'add',
			'skill_set' => $skill_set
		), 'admin', 'career');
	}

	public function edit($id)
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$career = $this->m_career->getData($id);

		if(empty($career)){
			redirect( base_url('career') );
		}else{
			$this->load->library('Layouts');
			$this->load->library('FileUpload');
		}

		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$FileUpload = new FileUpload();

			$name_career 	= $this->input->post('name_career');
			$location 		= $this->input->post('location');
			$type_job 		= $this->input->post('type_job');
			$description 	= $this->input->post('description');
			$status 		= $this->input->post('status');
			$skill_name 	= $this->input->post('skill_name');
			$skill_value 	= $this->input->post('skill_value');
			$image 			= $_FILES['image'];

			if(empty($status)){
				$status = 0;
			}

			$this->form_validation->set_rules('name_career', 'Career name', 'required');
			$this->form_validation->set_rules('type_job', 'Type job', 'required');

			$temp_skill_set = array();
			if(!empty($skill_name)){
				foreach ($skill_name as $key => $value) {
					$temp_skill_set[] = array(
						'name' => $value,
						'value' => ($skill_value[$key]) ? $skill_value[$key] : 0
					);
				}
			}

			$skill_set = json_encode($temp_skill_set);

			if ($this->form_validation->run() == TRUE){
				$data = array(
					'name_career' 	=> $name_career,
					'location' 		=> $location,
					'type_job' 		=> $type_job,
					'description' 	=> $description,
					'skill_set' 	=> $skill_set,
					'status' 		=> $status,
				);

				if(!empty($image['name'])){
					$image = $FileUpload->_uploadImage('image', 'career');

					if($image['status'] == 'success'){
						if(!empty($image['file_name'])){
							$data['image'] = $image['file_name'];
						}
					}else{
						$this->session->set_flashdata('warning', $image['message']);
					}
				}


				$result = $this->m_career->update($id, $data);
				
				if($result){
					$this->session->set_flashdata('success', 'Successfully changed data');

					redirect( base_url('career') );
				}else{
					$this->session->set_flashdata('error', 'Failed to change data');
				}

			}
		}


		$Layouts = new Layouts();
		
		$Layouts->add_include('assets/css/jquery.fancybox.min.css', true);
		$Layouts->add_include('assets/js/vendor/fancybox/jquery.fancybox.min.js', true);

		$Layouts->set_title('Edit Career');
		$Layouts->view('career/form', array(
			'action' 	=> 'edit/'.$id,
			'id' 		=> $id,
			'skill_set' => $career->skill_set,
			'career' 	=> $career,
			'include'	=> $Layouts->print_includes()
		), 'admin', 'career');
	}

	public function delete_image($id)
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$result = $this->m_career->delete_image($id);

		if(!empty($result)){
			$result = array(
				'status' => 'success',
				'message' => 'Successfully deleted image'
			);
		}else{
			$result = array(
				'status' => 'error',
				'message' => 'Failed to delete image'
			);
		}

		echo json_encode($result);
	}

	public function delete($id)
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$result = $this->m_career->delete($id);

		if(!empty($result)){
			$this->session->set_flashdata('success', 'Successfully deleted data');
		}else{
			$this->session->set_flashdata('error', 'Failed to delete data');
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function add_info($id)
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );

		$this->load->library('Layouts');
		$this->load->helper('form');

		$career = $this->m_career->getData($id);

		$skill_set 		= '';
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$label 						= $this->input->post('label');
			$description_information 	= $this->input->post('description_information');
			$order_row 					= $this->input->post('order_row');

			$this->form_validation->set_rules('label', 'Label', 'required');
			$this->form_validation->set_rules('description_information', 'description', 'required');

			if ($this->form_validation->run() == TRUE){
				$data = array(
					'id_career' 				=> $id,
					'label' 					=> $label,
					'description_information' 	=> $description_information,
					'order_row' 				=> $order_row,
				);

				$result = $this->m_career->insert_info($data);
				
				if($result){
					$this->session->set_flashdata('success', 'Successfully added data');

					redirect( base_url('career') );
				}else{
					$this->session->set_flashdata('error', 'Failed to add data');
				}

			}
		}

		$Layouts = new Layouts();

		$Layouts->add_include('assets/js/vendor/ckeditor/ckeditor.js', true);

		$include = $Layouts->print_includes();

		$Layouts->set_title('Add Info Career');
		$Layouts->view('career/form_info', array(
			'action' 	=> 'add_info/'.$id,
			'career' 	=> $career,
			'include'	=> $include
		), 'admin', 'career');
	}

	public function edit_info($id)
	{
		$this->load->library('Layouts');
		$this->load->helper('form');

		$career = $this->m_career->getDataInformation($id);

		$skill_set 		= '';
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$label 						= $this->input->post('label');
			$description_information 	= $this->input->post('description_information');
			$order_row 					= $this->input->post('order_row');

			$this->form_validation->set_rules('label', 'Label', 'required');
			$this->form_validation->set_rules('description_information', 'description', 'required');

			if ($this->form_validation->run() == TRUE){
				$data = array(
					'label' 					=> $label,
					'description_information' 	=> $description_information,
					'order_row' 				=> $order_row,
				);

				$result = $this->m_career->update_info($id, $data);
				
				if($result){
					$this->session->set_flashdata('success', 'Successfully changed data');

					redirect( base_url('career') );
				}else{
					$this->session->set_flashdata('error', 'Failed to change data');
				}

			}
		}

		$Layouts = new Layouts();

		$Layouts->add_include('assets/js/vendor/ckeditor/ckeditor.js', true);

		$include = $Layouts->print_includes();

		$Layouts->set_title('Add Info Career');
		$Layouts->view('career/form_info', array(
			'action' 	=> 'edit_info/'.$id,
			'career' 	=> $career,
			'include'	=> $include
		), 'admin', 'career');
	}

	public function delete_info($id)
	{
		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );
		
		$result = $this->m_career->delete_info($id);

		if(!empty($result)){
			$this->session->set_flashdata('success', 'Successfully deleted data');
		}else{
			$this->session->set_flashdata('error', 'Failed to delete data');
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function ajx_career_info($id)
	{
		$this->load->library(array('Layouts', 'Recaptcha', 'FileUpload'));
		$this->load->model('m_career');

		$career = $this->m_career->getData($id);
		$career->information = $this->m_career->data_information($id);

		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$view = 'career/apply_form';

			$FileUpload = new FileUpload();

			/* config fileupload */
			$FileUpload->allowed_types = 'pdf|doc|docx';
			$FileUpload->max_size = 4096;
			/* config fileupload */

			$name 	= $this->input->post('name');
			$phone 	= $this->input->post('phone');
			$email 	= $this->input->post('email');

			$this->form_validation->set_rules('name', 'Full Name', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required|numeric');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

			if ($this->form_validation->run() == TRUE){
				$captcha_answer = $this->input->post('g-recaptcha-response');
				$response = $this->recaptcha->verifyResponse($captcha_answer);

				if ($response['success']){
					$files = $FileUpload->_uploadImage('file_0', 'files_cv');
					
					if($files['status'] == 'success'){
						$this->load->model(array('m_setting'));
						$this->load->library('SendEmail');

						$SendEmail = new SendEmail();

						$setting = $this->m_setting->getData(array('email-contact'));

						$to = 'yohanes@asi-asiapacific.com';
						if(!empty($setting['email-contact'])){
							$to = $setting['email-contact'];
						}

						$data = array(
							'name' 		=> $name,
							'id_career' => $id,
							'phone' 	=> $phone,
							'email' 	=> $email,
							'files' 	=> $files['file_name'],
							'created' 	=> date('Y-m-d H:i:s'),
						);

						$result 	= $this->m_career->insert_apply($data);

						$subject = sprintf('Apply Job: %s', $career->name_career);

						$send_email = $SendEmail->send($to, $subject, 'apply_job', $data);
						
						if($result && $send_email['status'] == 'success'){
							$this->session->set_flashdata('success', 'Thank you for your application, we will do a review first');

							redirect( base_url('career/ajx_career_info/'.$id) );
						}else{
							$this->session->set_flashdata('error', 'Failed to submit application');
						}
					}else{
						$this->session->set_flashdata('error', $files['message']);
					}
				}else{
					$this->session->set_flashdata('error', 'Captcha Invalid');
				}
			}else{
				$this->session->set_flashdata('error', 'Failed to submit application');
			}
		}else{
			$view = 'career/career_info';
		}

		$data = array(
			'id' => $id,
			'career' => $career,
			'captcha' => $this->recaptcha->getWidget(), 
			'script_captcha' => $this->recaptcha->getScriptTag()
		);

		$Layouts = new Layouts();

		$Layouts->view($view, $data, 'ajax');
	}
}