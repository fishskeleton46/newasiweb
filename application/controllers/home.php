<?php 
class Home extends CI_Controller
{
	
	function __construct(){
		parent::__construct();

		if( $this->session->userdata("status") != "login" ) redirect( base_url('login') );
	}

	public function index()
	{
		$this->load->library('Layouts');
		$Layouts = new Layouts();

		$Layouts->set_title('Home');
		$Layouts->view('home', array());
	}
}