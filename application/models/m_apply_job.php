<?php 
	class m_apply_job extends CI_Model
	{
		protected $condition;
		protected $search_field = array('name','phone','email');

		function setCondition($keyword = '')
		{
			if(!empty($keyword)){
				foreach ($this->search_field as $key => $value) {
					$this->condition[$value] = $keyword;
				}
			}	
		}

		private function setWhereCondition($q)
		{
			$idx = 1;
			foreach ($this->condition as $field => $value) {
				if($idx == 1){
					$q->like($field, $value);

					$idx++;
				}else{
					$q->or_like($field, $value);
				}
			}

			return $q;
		}

		function data($per_page, $from_segment){
			$q = $this->db;

			if(!empty($this->condition)){
				$q = $this->setWhereCondition($q);
			}

			return $query = $q->order_by('created', 'desc')->get('job_apply', $per_page, $from_segment)->result();
		}

		function jumlah_data(){
			$q = $this->db;

			if(!empty($this->condition)){
				$q = $this->setWhereCondition($q);
			}

			return $q->get('job_apply')->num_rows();
		}

		public function getData($id)
		{
			$result = $this->db->where('id_job_apply', $id)->get('job_apply')->row();

			return $result;
		}

		public function hasRead($id)
		{
			$this->db->where('id', $id);
		
			return $this->db->update('job_apply', array(
				'has_read' => 1
			));
		}
	}
 ?>