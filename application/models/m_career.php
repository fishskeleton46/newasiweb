<?php 

class m_career extends CI_Model 
{
	protected $condition;
	protected $search_field = array('name_career', 'location', 'type_job', 'description');

	function setCondition($keyword = '')
	{
		if(!empty($keyword)){
			foreach ($this->search_field as $key => $value) {
				$this->condition[$value] = $keyword;
			}
		}	
	}

	private function setWhereCondition($q)
	{
		$idx = 1;
		foreach ($this->condition as $field => $value) {
			if($idx == 1){
				$q->like($field, $value);

				$idx++;
			}else{
				$q->or_like($field, $value);
			}
		}

		return $q;
	}

	public function getAllData()
	{
		$q = $this->db
			->where('status', 1)
			->order_by('created', 'desc')
			->get('career')
			->result();

		if(!empty($q)){
			foreach ($q as $key => $value) {
				$ref =& $q[$key];

				$ref->information = $this->data_information($value->id_career);
			}
		}

		return $q;
	}

	function data(){
		$this->load->library('PaginationCustom');
		
		$paginate = new PaginationCustom();

		$q = $this->db;

		if(!empty($this->condition)){
			$q = $this->setWhereCondition($q);
		}

		return $query = $q->order_by('created', 'desc')->get('career', $paginate->config['per_page'], $paginate->from_segment)->result();
	}

	public function data_information($id_career)
	{
		return $this->db->select(array('id_career_information', 'label', 'description_information'))
			->order_by('order_row', 'asc')
			->where('id_career', $id_career)->get('career_information')->result();
	}
 
	function jumlah_data(){
		$q = $this->db;

		if(!empty($this->condition)){
			$q = $this->setWhereCondition($q);
		}

		return $q->get('career')->num_rows();
	}

	public function skillSetList()
	{
		$result = $this->db->get('career_skill_set')->result();

		$temp = array();
		if(!empty($result)){
			foreach ($variable as $key => $value) {
				$temp[] = $value['name'];
			}
		}

		$result = $temp;

		return $result;
	}

	public function insert($arr_value)
	{
		return $this->db->insert('career', $arr_value);
	}

	public function update($id, $arr_value)
	{
		$this->db->where('id_career', $id);
		
		return $this->db->update('career', $arr_value);
	}

	public function delete($id)
	{
		$this->db->where('id_career', $id);
		
		return $this->db->delete('career');
	}

	public function getData($id)
	{
		$result = $this->db->where('id_career', $id)->get('career')->row();

		return $result;
	}

	public function getDataInformation($id)
	{
		$result = $this->db->where('id_career_information', $id)
			->join('career', 'career.id_career = career_information.id_career')
			->get('career_information')->row();

		return $result;
	}

	public function delete_image($id)
	{
		$this->db->where('id_career', $id);
		
		return $this->db->update('career', array(
			'image' => ''
		));
	}

	public function insert_info($arr_value)
	{
		return $this->db->insert('career_information', $arr_value);
	}

	public function update_info($id, $arr_value)
	{
		$this->db->where('id_career_information', $id);
		
		return $this->db->update('career_information', $arr_value);
	}

	public function delete_info($id)
	{
		$this->db->where('id_career_information', $id);
		
		return $this->db->delete('career_information');
	}

	public function insert_apply($arr_value)
	{
		return $this->db->insert('job_apply', $arr_value);
	}
}