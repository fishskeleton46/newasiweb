<?php 
	class m_contact extends CI_Model
	{
		protected $condition;
		protected $search_field = array('name','job_title','company','phone','email');

		function setCondition($keyword = '')
		{
			if(!empty($keyword)){
				foreach ($this->search_field as $key => $value) {
					$this->condition[$value] = $keyword;
				}
			}	
		}

		private function setWhereCondition($q)
		{
			$idx = 1;
			foreach ($this->condition as $field => $value) {
				if($idx == 1){
					$q->like($field, $value);

					$idx++;
				}else{
					$q->or_like($field, $value);
				}
			}

			return $q;
		}

		function data(){
			$this->load->library('PaginationCustom');
			
			$paginate = new PaginationCustom();

			$q = $this->db;

			if(!empty($this->condition)){
				$q = $this->setWhereCondition($q);
			}

			return $query = $q->order_by('created_at', 'desc')->get('contact', $paginate->config['per_page'], $paginate->from_segment)->result();
		}

		public function insert($arr_value)
		{
			return $this->db->insert('contact', $arr_value);
		}

		function jumlah_data(){
			$q = $this->db;

			if(!empty($this->condition)){
				$q = $this->setWhereCondition($q);
			}

			return $q->get('contact')->num_rows();
		}

		public function getData($id)
		{
			$result = $this->db->where('id_contact', $id)->get('contact')->row();

			return $result;
		}

		public function hasRead($id)
		{
			$this->db->where('id_contact', $id);
		
			return $this->db->update('contact', array(
				'has_read' => 1
			));
		}
	}
 ?>