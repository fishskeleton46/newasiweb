<?php

class M_home extends CI_Model {

	// 2019-06-18 | Lugito
	public function insertContact($data){

		$this->load->library('user_agent');
		$data['created_at'] 		= date('Y-m-d H:i:s');
		$data['browser']				= $this->agent->browser();
		$data['browserVersion']	= $this->agent->version();
		$data['platform']				= $this->agent->platform();
		$data['is_mobile']			= $this->agent->is_mobile();
		$data['is_robot']				= $this->agent->is_robot();
		$data['is_browser']			= $this->agent->is_browser();

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['X-Real-IP'])) {
			$ip = $_SERVER['X-Real-IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			$ip = preg_replace("/,.*/", "", $ip); # hosts are comma-separated, client is first
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		$geo_loc = json_decode(file_get_contents("https://ipinfo.io/" . $ip . "/json"), true);
		$isp = "";
		if (array_key_exists("org", $geo_loc)){
			$isp .= $geo_loc["org"];
			$isp=preg_replace("/AS\d{1,}\s/","",$isp); //Remove AS##### from ISP name, if present
		}else{
			$isp .= "Unknown ISP";
		}
		$data['geo_ip']        	= $ip;
		if (count($geo_loc) > 2){
			$data['geo_city']			= $geo_loc['city'];
			$data['geo_region']		= $geo_loc['region'];
			$data['geo_country']	= $geo_loc['country'];
			$data['geo_loc']			= $geo_loc['loc'];
			$data['geo_org']			= $geo_loc['org'];
			$data['geo_isp']			= $isp;
		}
		$geo_loc = json_decode(file_get_contents("https://tools.keycdn.com/geo.json?host=" . $ip), true);
		if ($geo_loc['status'] == "success") {
			$data['geo2_city']			= $geo_loc['data']['geo']['city'];
			$data['geo2_region']		= $geo_loc['data']['geo']['region_name'];
			$data['geo2_country']		= $geo_loc['data']['geo']['country_code'];
			$data['geo2_loc']				= $geo_loc['data']['geo']['latitude'] . "," . $geo_loc['data']['geo']['longitude'];
			$data['geo2_timezone']	= $geo_loc['data']['geo']['timezone'];
			$data['geo2_asn']				= $geo_loc['data']['geo']['asn'];
			$data['geo2_isp']				= $geo_loc['data']['geo']['isp'];
		}
		$this->db->insert('contact', $data);

		return $this->db->insert_id();

	}
}
