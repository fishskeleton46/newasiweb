<?php 

class User extends CI_Model 
{
	
	function check_login($username, $password)	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$get = $this->db->get('admin');
		if($get->num_rows() > 0) return $get->row();
		else return FALSE;
	}

	function check_password($id, $password)	{
		$this->db->where('id', $id);
		$this->db->where('password', $password);
		$get = $this->db->get('admin');
		if($get->num_rows() > 0) return $get->row();
		else return FALSE;
	}

	public function update_password($id, $new_password)
	{
		$this->db->where('id', $id);
		
		return $this->db->update('admin', array(
			'password' => md5($new_password)
		));
	}
}