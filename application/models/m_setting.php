<?php 
	class m_setting extends CI_Model
	{
		
		public function general_setting($arr_value)
		{
			if(!empty($arr_value)){
				foreach ($arr_value as $key => $value) {
					$db = $this->db;
					
					$db
						->where('slug', $key)
						->update('setting', array(
							'value' => $value
						));
				}
			}
		}

		public function getData($arr_slug = array())
		{
			$result = $this->db->where_in('slug', $arr_slug)->get('setting')->result();;

			$temp = array();
			foreach ($result as $key => $value) {
				$temp[$value->slug] = $value->value;
			}

			$result = $temp;

			return $result;
		}
	}
 ?>